try:
    from setuptools import setup
    from setuptools import find_packages
    packages = find_packages()
except ImportError:
    from distutils.core import setup
    import os
    packages = [x.strip('./').replace('/','.') for x in os.popen('find -name "__init__.py" | xargs -n1 dirname').read().strip().split('\n')]

if bytes is str:
    raise Exception("This module is designed for python 3 only. Please install an older version to use python 2.")

setup(
    name='BATL',
    version='0.1',
    python_requires='>=3.5',
    packages=packages,
    install_requires=[
        'numpy==1.15.4',
        'z3-solver==4.5.1.0.post2',
    ],
    description='A collection of tools for analysis of TBL',
    url='https://github.ccs.neu.edu/konathan/BATL',
)
