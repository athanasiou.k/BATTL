import pickle
import pdb
import re
import IPython
import json
import datetime

def get_lps(plp_file):
    with open(plp_file, 'rb') as f:
        tbl_leaky_points, vbl_leaky_points = pickle.load(f)
    return tbl_leaky_points, vbl_leaky_points


def get_tbl(plp_file, id):
    with open(plp_file, 'rb') as f:
        res = None
        tbl_leaky_points, _ = pickle.load(f)
        for tbl in tbl_leaky_points:
            if tbl.lp_id == id:
                res= tbl
    IPython.embed()
    return res

def get_vbl(plp_file, id):
    with open(plp_file, 'rb') as f:
        res = None
        _, vbl_leaky_points = pickle.load(f)
        for vbl in vbl_leaky_points:
            if vbl.lp_id == id:
                res= vbl
    IPython.embed()
    return res

def print_tblps(plp_file, genuine_only=False):
    with open(plp_file, 'rb') as f:
        tbl_leaky_points, _ = pickle.load(f)
    
    print("TBL")
    print('{0:<6}|{1:<35}|{2:<18}|{3:<9}|{4:<10}|{5:<5}|{6:<5}|{7:<10}'.format('LP-ID', 'inst','read set', 'write reg', 'new addr', 'vblps', 'cls', 'ila'))
    print('-'*100)
    for lp in tbl_leaky_points:
        var_num = len(lp.lk_expr.variables)
        if genuine_only and hasattr(lp, 'ila') and (var_num >=4 or lp.ila ==0):
            continue
        if hasattr(lp, 'ila') and var_num >= 4:
            ila_val = str(lp.ila) + "*"
        elif hasattr(lp, 'ila'):
            ila_val = lp.ila
        else:
            ila_val = "-"
        if hasattr(lp, 'leak_class'):
            leak_class = lp.leak_class
        else:
            leak_class = "-"
        print('{0:<6}|{1:<35}|{2:<18}|{3:<9}|{4:<10}|{5:<6}|{6:<6}|{7:<10}'.format(
            lp.lp_id,
            lp.insn_str,
            ','.join(lp.read_set),
            lp.reg_name,
            hex(lp.new_write_addr),
            (str(lp.old_write_vblp) if lp.old_write_vblp else "-")+","+
            (str(lp.new_write_vblp) if lp.new_write_vblp else "-"),
            leak_class,
            ila_val))

def print_vblps(plp_file, genuine_only=False):
    with open(plp_file, 'rb') as f:
        _, vbl_leaky_points = pickle.load(f)
    
    print("VBL")
    print('{0:<6}|{1:<35}|{2:<18}|{3:<9}|{4:<10}|{5:<5}|{6:<30}'.format('LP-ID', 'inst','read set', 'write reg', 'new addr', 'cls','ila'))
    print('-'*80)
    for lp in vbl_leaky_points:
        var_num = len(lp.lk_expr.variables)
        if genuine_only and hasattr(lp, 'ila') and (var_num >=4 or lp.ila ==0):
            continue
        if hasattr(lp, 'ila') and var_num >=4:
            ila_val = str(lp.ila) + "*"
        elif hasattr(lp, 'ila'):
            ila_val = lp.ila
        else:
            ila_val = "-"
        if hasattr(lp, 'leak_class'):
            leak_class = lp.leak_class
        else:
            leak_class = "-"
        print('{0:<6}|{1:<35}|{2:<18}|{3:<9}|{4:<10}|{5:<5}|{6:<30}'.format(
            lp.lp_id,
            lp.insn_str,
            ','.join(lp.read_set),
            lp.reg_name,
            hex(lp.new_write_addr),
            leak_class,
            ila_val))

def print_plps(plp_file, genuine_only=False, plp_type=None):
    if not plp_type:
        print_vblps(plp_file, genuine_only)
        print_tblps(plp_file, genuine_only)
    elif plp_type=='tbl':
        print_tblps(plp_file, genuine_only)
    elif plp_type=='vbl':
        print_tblps(plp_file, genuine_only)

def print_plp_table(plp_file_format, profile_file_format):

    cc = ['gnu', 'llvm']
    opt = ['O0', 'O1', 'O2']
    res_dict = {'gnu' : {'O1':[], 'O2':[], 'O0':[]},
                'llvm' : {'O1':[], 'O2':[], 'O0':[]}}
    for c in cc:
        for o in opt:

            gtbls_num = 0
            rstbls_num = 0
            ptbls_num = 0
            ila0_num = 0
            plp_file = plp_file_format.format(c, o) 
            with open(plp_file, 'rb') as f:
                ptbls, pvbls = pickle.load(f)
            print("opened",plp_file)
            for tbl in ptbls:
                if tbl.new_write_vblp:
                    vbl = pvbls[tbl.new_write_vblp]
                    if hasattr(vbl, 'ila') and vbl.ila > 0:
                        continue
                if tbl.old_write_vblp:
                    vbl = pvbls[tbl.old_write_vblp]
                    if hasattr(vbl, 'ila') and vbl.ila > 0:
                        continue
                ptbls_num += 1
                if hasattr(tbl, 'leak_class') and (tbl.leak_class=="RUD" or tbl.leak_class == "SID"):
                    rstbls_num += 1
                    continue
                if hasattr(tbl, 'ila') and (tbl.ila > 0):
                    gtbls_num += 1
                    continue
                if hasattr(tbl, 'ila') and (tbl.ila == 0):
                    ila0_num += 1
                    continue
                raise Exception('something wrong with ptbls')
            res_dict[c][o] = [ptbls_num, rstbls_num, ila0_num, gtbls_num]


    table_columns = []
    for c in cc:
        for o in opt:
            column = ["%"+c+o]
            for v in zip(res_dict[c][o]):
                column.append(*v)
            table_columns.append(column)
                
    for col in zip(*table_columns):
        print(" & ".join(list(map(str, col)))+" \\\\")

    ## Runtime info

    run_res_dict = {'gnu' : {'O1':[], 'O2':[], 'O0':[]},
                'llvm' : {'O1':[], 'O2':[], 'O0':[]}}
            

    for c in cc:
        for o in opt:
            profile_file = profile_file_format.format(c, o) 
            with open(profile_file) as jsonf:
                data = json.load(jsonf)
            print("opened",profile_file)

            symex_t = data['symex_t']
            rules_t = data['rules_t']
            ila_total = 0
            for k in data:
                if k == 'symex_t' or k == 'rules_t':
                    continue
                ila_total += data[k]
            col = list(map(lambda x: str(datetime.timedelta(seconds=round(x)))+"s", [symex_t, rules_t, ila_total]))
            col = list(map(lambda x: x.replace(":", "h", 1), col))
            run_res_dict[c][o] = list(map(lambda x: x.replace(":", "m", 1), col))

    table_columns = []
    for c in cc:
        for o in opt:
            column = ["%"+c+o]
            for v in zip(run_res_dict[c][o]):
                column.append(*v)
            table_columns.append(column)
                
    for col in zip(*table_columns):
        print(" & ".join(list(map(str, col)))+" \\\\")

def print_plp_stats(plp_file):
    with open(plp_file, 'rb') as f:
        ptbls, pvbls = pickle.load(f)

    gtbls = 0
    gvbls = 0
    rstbls = 0
    for ptbl in ptbls:
        if hasattr(ptbl, 'leak_class') and (ptbl.leak_class=="RUD" or ptbl.leak_class == "SID"):
            rstbls += 1
        if hasattr(ptbl, 'ila') and ptbl.ila > 0:
            gtbls += 1

    print("# PTBL: {0}".format(len(ptbls)))
    print("# RUD/SID: {0}".format(rstbls))
    print("# GTBL: {0}".format(gtbls))
        
def print_plp_stats_old(plp_file):
    with open(plp_file, 'rb') as f:
        ptblps, pvblps = pickle.load(f)

    gtblps = 0
    gstblps = 0
    gvblps = 0
    gsvblps = 0
    for ptblp in ptblps:
        if not hasattr(ptblp, 'ila'):
            print("No ila val for PTBLP {0}".format(ptblp.lp_id))
            continue
        ila_val = ptblp.ila
        var_num = len(ptblp.lk_expr.variables)
        if var_num < 4 and ila_val > 0:
            gtblps += 1
        elif var_num >= 4 and ila_val > 0:
            gstblps += 1

    for pvblp in pvblps:
        if not hasattr(pvblp, 'ila'):
            print("No ila val for PVBLP {0}".format(pvblp.lp_id))
            continue
        ila_val = pvblp.ila
        var_num = len(pvblp.lk_expr.variables)
        if var_num < 4 and ila_val > 0:
            gvblps += 1
        elif var_num >= 4 and ila_val > 0:
            gsvblps += 1

    header = [' ', 'Potential', 'Genuine', 'Sampling non zero']
    VBL = ['VBL', len(pvblps), gvblps, gsvblps]
    TBL = ['TBL', len(ptblps), gtblps, gstblps]


    s = [[str(e) for e in row] for row in [header, VBL, TBL]]
    lens = [max(map(len, col)) for col in zip(*s)]
    fmt = '\t'.join('{{:{}}}'.format(x) for x in lens)
    table = [fmt.format(*row) for row in s]
    print('\n'.join(table))

    # print("PVBLPs:\t{0}".format(len(pvblps)))
    # print("PTBLPs:\t{0}".format(len(ptblps)))
    # print("GVBLPs:\t{0}".format(gvblps))
    # print("GTBLPs:\t{0}".format(gtblps))
    # print("SVBLPs:\t{0}".format(gsvblps))
    # print("STBLPs:\t{0}".format(gstblps))
        

def print_plp_data(plp_file, leakage_type):
    with open(plp_file, 'rb') as f:
        ptblps, pvblps = pickle.load(f)


    try:
        found = re.search('share-(.+?)\/', plp_file).group(1)
    except AttributeError:
        found =''
    cnt = 0
    ila0 = 0
    ila10m5 = 0
    ilabig = 0

    #print("# CC ILA=0 ILA=10m5 ILA>10m Rule")
    for ptblp in ptblps:
        if not hasattr(ptblp, 'ila'):
            continue
        ila_val = ptblp.ila
        if ila_val == 0:
            ila0+=1
        if ila_val >0 and ila_val <  0.0001:
            ila10m5+=1
        if ila_val >= 0.0001:
            ilabig+=1
        cnt += 1
    if leakage_type=="GTBLs":
        if "gnu" in found:
            found=found.replace("gnu", "gcc")
        print(found, ila0, ila10m5, ilabig)
    cnt = 0
    ila0 = 0
    ila10m5 = 0
    ilabig = 0
    for pvblp in pvblps:
        if not hasattr(pvblp, 'ila'):
            continue
        ila_val = pvblp.ila
        if ila_val == 0:
            ila0+=1
        if ila_val >0 and ila_val <  0.0001:
            ila10m5+=1
        if ila_val >= 0.0001:
            ilabig+=1
        cnt += 1
    if leakage_type=="GVBLs":
        if "gnu" in found:
            found=found.replace("gnu", "gcc")
        print(found, ila0, ila10m5, ilabig)

        
def print_plp_scatter(plp_file, leakage_type, bm_cnt):
    with open(plp_file, 'rb') as f:
        ptblps, pvblps = pickle.load(f)


    try:
        found = re.search('share-(.+?)\/', plp_file).group(1)
    except AttributeError:
        found =''
    cnt = 0

    tbls=[]

    if "gnu" in found:
        found=found.replace("gnu", "gcc")
    print("\"{0}\"".format(found))

    bm_cnt = str(bm_cnt)+"."
    if leakage_type=="GTBLs":
        for ptblp in ptblps:
            if not hasattr(ptblp, 'ila'):
                continue
            var_num = len(ptblp.lk_expr.variables)
            ila_val = ptblp.ila
            if ila_val == 0:
                continue
#            ila_val = str(ila_val)+"*" if var_num > 3 else ila_val
            print(bm_cnt+str(cnt), ila_val)
            tbls.append(ila_val)
            cnt+=1
        print("\n")

    
    if leakage_type=="GVBLs":
        for pvblp in pvblps:
            if not hasattr(pvblp, 'ila'):
                continue
            ila_val = pvblp.ila
            if ila_val == 0:
                continue
            print(bm_cnt+str(cnt), ila_val)
            cnt+=1
        print("\n")





