import pickle
import os
from batl.dwarf_decode_address import process_file, bytes2str
import pdb

# returns the given list of registers but with translated synonyms, as they are defined here:
# http://www.keil.com/support/man/docs/armasm/armasm_dom1359731136117.htm
def normalize_reg_name(r):
    if r == "ip":
        return "r12"
    elif r == "fp":
        return "r11"
    elif r == "sl":
        return "r10"
    else:
         return r
    
def can_patch(insn):
    print("in canpatch", insn)
    cond_codes = ['ls', 'hi', 'cc', 'cs', 'lt']
    if '@' in insn:
        insn = insn.split('@', 1)[0]+"\n"
    insn_split = insn.split(', ')
    opc= insn_split[0].split('\t')[1]
    dst= insn_split[0].split('\t')[2]
    if opc[-2:] in cond_codes:
        return False
    else:
        return True


def get_insn_reads(insn):
    if '@' in insn:
        insn = insn.split('@', 1)[0]+"\n"
    insn_split = insn.split(', ')
    # if "add" in insn_split[0] and len(insn_split[1:]) == 1:
    #     pdb.set_trace()
    #     insn_reads = insn_split[1:]+[insn_split[0].split("\t")[-1]]
    # else:
    insn_reads = insn_split[1:]
    for i in range(len(insn_reads)):
        insn_reads[i] = normalize_reg_name(insn_reads[i].strip(",[]\n\t"))
    return insn_reads

## assumes an instruction of the form
## `opcode insn_args` is given. Will generate
## garbage on other things like symbols, comments, labels.
def insn_sub_read_reg(insn, read_reg, sub_reg):
    # print(insn, read_reg, sub_reg)
    print(insn)
#    pdb.set_trace()
    if '@' in insn:
        insn = insn.split('@', 1)[0]+"\n"
    insn_split = insn.split(', ')
    # print(insn_split)
    insn_args= insn_split[1:]
    new_args = []
    for i_a in insn_args:
        if normalize_reg_name(i_a.strip(",\n")) == read_reg:
            new_args.append(sub_reg)
        elif normalize_reg_name(i_a.lstrip("[")) == read_reg:
            new_args.append("["+sub_reg)
        else:
            new_args.append(i_a)
    new_insn_split = insn_split[:1]+[', '.join(new_args)]
    new_insn = ', '.join(new_insn_split)
    if not new_insn.endswith("\n"):
        new_insn+="\n"
    print("{0} -> {1}".format(insn.strip('\n'), new_insn.strip('\n')))
    return new_insn

## llvm uses synonyms for update instructions of the form
## opc rd, rd, rt -> opc rd, rt
## Make sure they are of the former type.
def fix_asm_instructions(asm_file):
    with open(asm_file, 'r') as f:
        lines = f.readlines()

    for l_idx, l in enumerate(lines):
        if not 'add' in l:
            continue
        insn = l
        if '@' in insn:
            insn = insn.split('@', 1)[0]+"\n"
        insn_split = insn.split(', ')
        if len(insn_split[1:]) == 1:
            hidden_read = insn_split[0].split("\t")[-1]
            insn_split.insert(1, hidden_read)
            new_inst = ', '.join(insn_split)
            lines[l_idx] = new_inst
            print(l_idx, l, new_inst)

    with open(asm_file, 'w') as f:
        for line in lines:
            f.write(line)

    # if '@' in insn:
    #     insn = insn.split('@', 1)[0]+"\n"
    # insn_split = insn.split(', ')
    # if "add" in l and len(insn_split[1:]) == 1:
    #     pdb.set_trace()
    #     insn_reads = insn_split[1:]+[insn_split[0].split("\t")[-1]]
    # else:
    #     insn_reads = insn_split[1:]
    # for i in range(len(insn_reads)):
    #     insn_reads[i] = normalize_reg_name(insn_reads[i].strip(",[]\n\t"))
    # return insn_reads

    # for l in lines:
        
# Assumes all lps are in the same file. Attemps to patch all at once.
# genuine_only indicates to patch all ptblps with exact ila>0.
def patch_lps(plp_file, elf_file, genuine_only=False):
    lines = []
    with open(plp_file, 'rb') as f:
        tbl_leaky_points, vbl_leaky_points = pickle.load(f)
    
    tbl_lp = tbl_leaky_points[0]
    file_name, _ = process_file(elf_file, tbl_lp.new_write_addr)
    asm_file = os.path.join(os.path.dirname(elf_file), file_name.decode('utf-8'))
    fix_asm_instructions(asm_file)
    print(asm_file)
    tbl_lines = []
    tbl_regs = []
    tbl_reads = []
    tbl_patches = []
    tbl_visits = []
    for lp in tbl_leaky_points:
        if not hasattr(lp, 'ila'):
            print("PTBLP{0} shown to be SID or due to VBL. Not patching it".format(lp.lp_id))
            continue
        if genuine_only:
            if lp.ila > 0: # and len(lp.lk_expr.variables) < 4:
                tbl_file, tbl_line = process_file(elf_file, lp.new_write_addr)
                print("PTBLP{0} will be patched: {1} @ Line {2}".format(lp.lp_id, tbl_file, tbl_line))
                # One patch/GTBL fix per line
                if tbl_line not in tbl_lines:
                    tbl_lines.append(tbl_line)
                    tbl_regs.append(lp.reg_name)
                    tbl_reads.append(lp.read_set)
                    tbl_patches.append(lp.patch_reg)
                    tbl_visits.append(lp.total_visit_cnt)
        ## This functionality is probably useless now
        else:
            tbl_lines.append(tbl_line)
            tbl_regs.append(lp.reg_name)
            tbl_reads.append(lp.read_set)
            tbl_patches.append(lp.patch_reg)
    linesxregs = sorted(zip(tbl_lines, tbl_regs, tbl_reads, tbl_patches, tbl_visits))
    with open(asm_file) as fp:
        tbl_iter = iter(linesxregs)
        tbl_line, tbl_reg, tbl_reads, tbl_patch_reg, tbl_visit_cnt = next(tbl_iter)
        test_cnt = 0
        for line_idx, line in enumerate(fp):
            if (tbl_line-1) == line_idx:
                test_cnt+=1
                print(test_cnt)
                insn_reads = get_insn_reads(line)
                print(tbl_reads, insn_reads)
                tbl_reads = [read for read in tbl_reads if read in insn_reads]
                print(tbl_reads)
                # print(tbl_reg, tbl_reads, tbl_patch_reg)
                if not can_patch(line):
                    print("Warning: Cannot patch{0}".format(line))
                    lines.append(line)
                elif not tbl_reg in tbl_reads:
                    print("Applying flushing instruction to {0} at line {1} ({2} visits)".format(line.strip("\n"), line_idx, tbl_visit_cnt))
                    lines.append('@ flushing instruction (#{0}) with {1} visits\n'.format(test_cnt, tbl_visit_cnt))
                    lines.append('\tmov\t{0}, #0\n'.format(tbl_reg))
                    lines.append(line)
                else:
                    print("Applying flushing gadget of instrction {0} at line {1} ({2} visits)".format(line.strip("\n"), line_idx, tbl_visit_cnt))
                    if tbl_patch_reg is None:
                        print("Spilling required...")
                        lines.append('@ flushing gadget (#{0}) for {1} w/ spilling ({2} visits)\n'.format(test_cnt, line.strip("\n"), tbl_visit_cnt))
                        lines.append('\tpush\t{r8}\n')
                        lines.append('\tmov\tr8, #0\n')
                        lines.append('\tmov\tr8, {0}\n'.format(tbl_reg))
                        lines.append('\tmov\t{0}, #0\n'.format(tbl_reg))
                        lines.append(insn_sub_read_reg(line, tbl_reg, "r8"))
                        lines.append('\tmov\tr8, #0\n')
                        lines.append('\tpop\t{r8}\n')
                    else:
                        print("Patch reg:", tbl_patch_reg)
                        tpr = tbl_patch_reg
                        lines.append('@ flushing gadget (#{0}) for {1} ({2} visits)\n'.format(test_cnt, line.strip("\n"), tbl_visit_cnt))
                        lines.append('\tmov\t{0}, #0\n'.format(tpr))
                        lines.append('\tmov\t{0}, {1}\n'.format(tpr, tbl_reg))
                        lines.append('\tmov\t{0}, #0\n'.format(tbl_reg))
                        lines.append(insn_sub_read_reg(line, tbl_reg, tpr))
                        lines.append('\tmov\t{0}, #0\n'.format(tpr))
                try:
                    tbl_line, tbl_reg, tbl_reads, tbl_patch_reg, tbl_visit_cnt = next(tbl_iter)
                except StopIteration:
                    print("Applied all patches")
            else:
                lines.append(line)
    with open(asm_file, 'w') as s_file:
        for line in lines:
            s_file.write(line)


