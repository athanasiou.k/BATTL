import claripy.ast.base
import operator
import claripy

'''
This file contains trivial printing functions on claripy's ast.
'''

vals = {}


def ast_eval(ast):
    if isinstance(ast, claripy.ast.Base):
        if ast.op == 'ZeroExt':
            print 'ZeroExt', len(ast.args)
            return ast_eval(ast.args[1])
        elif ast.op == 'BVV':
            print 'BVV', len(ast.args)
            return ast.args[0]
        elif ast.op == 'BVS':
            print 'BVS:', ast.args[0]
            return vals[ast.args[0]]
        elif ast.op == '__and__':
            print '__and__', len(ast.args)
            return reduce(operator.__and__, map(ast_eval, ast.args), 255)
        elif ast.op == '__or__':
            print '__or__', len(ast.args)
            return reduce(operator.__or__, map(ast_eval, ast.args), 0)
        elif ast.op == '__xor__':
            print "__xor__", len(ast.args)
            return reduce(operator.__xor__, map(ast_eval, ast.args), 0)
        elif ast.op == '__add__':
            print '__add__', len(ast.args)
            return reduce(operator.add, map(ast_eval, ast.args), 0)
        elif ast.op == '__sub__':
            print '__sub__', len(ast.args)
            return reduce(operator.sub, map(ast_eval, ast.args), 0)
        elif ast.op == '__mul__':
            print '__mul__', len(ast.args)
            return reduce(operator.mul, map(ast_eval, ast.args), 1)
        elif ast.op == '__eq__':
            print '__ge__', len(ast.args)
            return ast_eval(ast.args[0]) == ast_eval(ast.args[1])
        elif ast.op == '__ge__':
            print '__ge__', len(ast.args)
            return ast_eval(ast.args[0]) >= ast_eval(ast.args[1])
        elif ast.op == '__le__':
            print '__le__', len(ast.args)
            return ast_eval(ast.args[0]) <= ast_eval(ast.args[1])
        elif ast.op == '__lt__':
            print '__lt__', len(ast.args)
            return ast_eval(ast.args[0]) < ast_eval(ast.args[1])
        elif ast.op == '__gt__':
            print '__gt__', len(ast.args)
            return ast_eval(ast.args[0]) > ast_eval(ast.args[1])
        elif ast.op == 'If':
            print 'If', len(ast.args)
            if ast_eval(ast.args[0]):
                return ast_eval(ast.args[1])
            else:
                return ast_eval(ast.args[2])
        elif ast.op == 'Concat':
            return 2
        elif ast.op == 'LShR':
            print 'LShR', len(ast.args)
            return operator.rshift(
                ast_eval(ast.args[0]), ast_eval(ast.args[1]))
        elif ast.op == '__lshift__':
            print '__lshift__', len(ast.args)
            return operator.lshift(
                ast_eval(ast.args[0]), ast_eval(ast.args[1]))
        elif ast.op == '__rshift__':
            print '__rshift__', len(ast.args)
            return operator.rshift(
                ast_eval(ast.args[0]), ast_eval(ast.args[1]))
        else:
            print ast.op, "not implemented"
        for a in ast.args:
            ast_eval(a)


def ast_print(ast):
    if isinstance(ast, claripy.ast.Base):
        if hasattr(ast, 'alog'):
            print "following is alog"
        if ast.op == 'ZeroExt':
            print 'ZeroExt', len(ast.args)
            print "\t", ast.args[0]
            print "\t", ast.args[1]
        elif ast.op == 'BVS':
            print 'BVS:', ast.args[0]
        elif ast.op == '__and__':
            print '__and__', len(ast.args)
        elif ast.op == '__mul__':
            print '__mul__', len(ast.args)
        elif ast.op == 'If':
            print 'If', len(ast.args)
        elif ast.op == '__ge__':
            print '__ge__', len(ast.args)
        elif ast.op == '__add__':
            print '__add__', len(ast.args)
        elif ast.op == 'BVV':
            print 'BVV', len(ast.args)
        elif ast.op == '__gt__':
            print '__gt__', len(ast.args)
        elif ast.op == 'Concat':
            print 'Concat', len(ast.args)
            print "\t", ast.args[0]
            print "\t", ast.args[1]
        elif ast.op == 'LShR':
            print 'LShR', len(ast.args)
        elif ast.op == '__lshift__':
            print '__lshift__', len(ast.args)
        elif ast.op == '__invert__':
            print '__invert__', len(ast.args)
        elif ast.op == '__or__':
            print '__or__', len(ast.args)
        elif ast.op == '__sub__':
            print '__sub__', len(ast.args)
        elif ast.op == '__rshift__':
            print '__rshift__', len(ast.args)
        elif ast.op == '__xor__':
            print "__xor__", len(ast.args)
        else:
            print ast.op, "not implemented"
        for a in ast.args:
            ast_print(a)
