import pickle
import os
from batl.dwarf_decode_address import process_file, bytes2str
import pdb
import sys
from pycparser import c_parser, c_ast, parse_file

class FuncDefVisitor(c_ast.NodeVisitor):
    def __init__(self, target_func):
        self.target_func = target_func
        self.found_decl_line = None
        
    def visit_FuncDef(self, node):
        #print('%s at %s' % (node.decl.name, node.decl.coord))
        if self.target_func == node.decl.name:
            self.found_decl_line = node.decl.coord.line
            print(self.found_decl_line)

def patch_gvbl(plp_file, elf_file):
    with open(plp_file, 'rb') as f:
        tbl_leaky_points, vbl_leaky_points = pickle.load(f)

    funcs_to_patch = {}
    for gvbl in vbl_leaky_points:
        if gvbl.func_name not in funcs_to_patch:
            funcs_to_patch[gvbl.func_name]=gvbl.new_write_addr

    for func in funcs_to_patch:
        file_name, _ = process_file(elf_file, funcs_to_patch[func])
        source_file_name = "src/"+file_name.decode('utf-8')[:-1]+"c"
        # if not hasattr(gvbl, 'ila'):
        #     print("can't patch VBLP{0}: its has no ILA".format(gvbl.lp_id))
        # if gvbl.ila > 0:
        #     print("GVBLP{0} will be patched".format(gvbl.lp_id))

        #     file_name, _ = process_file(elf_file, gvbl.new_write_addr)
        #     source_file_name = sile_name[:-1]+"c"
            

        ast = parse_file(source_file_name, use_cpp=True, cpp_args=r'-Iinc/')
        v = FuncDefVisitor(gvbl.func_name)
        v.visit(ast)

        with open(source_file_name, 'r') as src_f:
            content = src_f.readlines()

        content[v.found_decl_line-1] = "__attribute__((optimize (\"no-tree-reassoc\"))) " + content[v.found_decl_line-1]

        with open(source_file_name, 'w') as p_src_f:
            for c in content:
                p_src_f.write(c)
