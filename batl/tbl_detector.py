import batl.translator
import angr
import batl.utils
from batl.dwarf_decode_address import process_file, bytes2str
import pdb
import os

class LeakyPoint(object):
    def __init__(self, lp_id, lk_expr, shares, old_write_addr, new_write_addr,
                 old_write_vblp, new_write_vblp, insn_str, reg_name, read_set,
                 func_name, visit_cnt, cnt):
        self.lp_id = lp_id
        self.lk_expr = lk_expr
        self.shares = shares
        self.old_write_addr = old_write_addr
        self.new_write_addr = new_write_addr
        self.old_write_vblp = old_write_vblp
        self.new_write_vblp = new_write_vblp
        self.reg_name = reg_name
        self.insn_str = insn_str
        self.read_set = read_set
        self.func_name = func_name
        self.visit_cnt = visit_cnt
        self.cnt = cnt
        self.patch_reg = None
        self.total_visit_cnt = None
        
class TBLPlugin(angr.SimStatePlugin):
    def __init__(self, bv0, shares, reg_names, vbl=False):
        self.shares = shares
        self.shares_vars = []
        for s in shares:
            i_shares = s[0].variables
            for i in range(1, len(s)):
                i_shares = i_shares.union(s[i].variables)
            self.shares_vars.append(i_shares)
        self.bv0 = bv0
        super(TBLPlugin, self).__init__()
        self.regs = {key: bv0 for key in reg_names.keys()}
        self.regs_last_write = {key: 0x8000000 for key in reg_names.keys()}
        self.tbl = {key: bv0 for key in reg_names.keys()}
        self.leaky_points = []
        self.id_cnt = 0
        self.vbl = vbl
        self.vbl_leaky_points = []
        self.vbl_id_cnt = 0
        # blacklist cc_deps reg for arm, and itstate
        self.regs_blacklist = [72, 76, 80, 84, 392]
        self.vbl_cache = {}
        self.gp_regs = { 8: 'r0', 12: 'r1', 16: 'r2', 20: 'r3', 24: 'r4',
                         28: 'r5', 32: 'r6', 36: 'r7', 40: 'r8', 44: 'r9',
                         48: 'r10', 52: 'r11', 56: 'r12', 64: 'lr' }
        self.gp_last_use = {key: 0 for key in self.gp_regs.keys()}
        
    @angr.SimStatePlugin.memo
    def copy(self, memo):
        copy = TBLPlugin(self.bv0, self.shares, {})
        copy.regs = self.regs
        copy.regs_last_write = self.regs_last_write
        copy.tbl = self.tbl
        copy.shares = self.shares
        copy.shares_vars = self.shares_vars
        copy.leaky_points = self.leaky_points
        copy.id_cnt = self.id_cnt
        copy.vbl = self.vbl
        copy.vbl_leaky_points = self.vbl_leaky_points
        copy.vbl_id_cnt = self.vbl_id_cnt
        copy.regs_blacklist = self.regs_blacklist
        copy.vbl_cache = self.vbl_cache
        copy.gp_regs = self.gp_regs
        copy.gp_last_use = self.gp_last_use

        return copy

    
def update_regs(state):
    offset = state.inspect.reg_write_offset
    old_v = state.registers.load(offset)
    state.tbl_plugin.regs[offset] = old_v


def insn_str_at_addr(addr, elf_file):
    file_name, line_num = process_file(elf_file, addr)
    file = os.path.join(os.path.dirname(elf_file), file_name.decode('utf-8'))
    fp = open(file)
    pp_inst = ''
    for i, line in enumerate(fp):
        if i == line_num-1:
            inst = line.split('@', 1)[0].strip().replace('\t', ' ')
            pp_inst = '{0:<7}{1:<18}'.format(inst.split(' ')[0], ' '.join(inst.split(' ')[1:]))
            break
    fp.close()
    return pp_inst

# def get_func_symbol_name():
#     for 


def update_tbl(state):

    # Update addr ticks
    offset = state.inspect.reg_write_offset


    # Update last use per gp_reg
    for e in state.history.recent_events:
        if e.type != 'reg' or e.offset not in state.tbl_plugin.gp_regs or e.action != 'read':
            continue
        state.tbl_plugin.gp_last_use[e.offset] = state.globals['cnt']

    ## The idea of "locally" finding dead regs doesn't work, since in
    ## different visits of the address, the reg might not be dead. So we
    ## need to keep track of dead regs every time we visit an address. At
    ## the end if this address gives a TBL and there exists a register that is (always) dead at this address,
    ## we use this register to patch
    for e in state.history.recent_events:
        if e.type != 'reg' or e.offset not in state.tbl_plugin.gp_regs or e.action != 'write':
            continue
        if state.tbl_plugin.gp_last_use[e.offset] == state.globals['cnt']:
            continue
        current_cnt = state.globals['cnt']
        last_use_cnt = state.tbl_plugin.gp_last_use[e.offset]
        print("reg {0} dead between [{1}({2}), {3}({4})]".format(state.tbl_plugin.gp_regs[e.offset],
                                                                 last_use_cnt,
                                                                 hex(state.globals['cnt2addr'][last_use_cnt]),
                                                                 current_cnt,
                                                                 hex(state.globals['cnt2addr'][current_cnt])))
        for i in range(last_use_cnt+1, current_cnt):
            addr = state.globals['cnt2addr'][i]
            #print(i, hex(addr))
            state.globals['addr2dead'][addr][-1].add(state.tbl_plugin.gp_regs[e.offset])

        
        # for lp in state.tbl_plugin.leaky_points:
        #     pdb.set_trace()
        #     if lp.cnt > state.tbl_plugin.gp_last_use[e.offset] and lp.cnt < state.globals['cnt']:
        #         print("reg{0} can be used to patch TBL{1}".format(state.tbl_plugin.gp_regs[e.offset], lp.lp_id))
        #         lp.patch_reg.add(state.tbl_plugin.gp_regs[e.offset])

    caps_insns_block = state.block().capstone.insns
    caps_insn = None
    for i in caps_insns_block:
        if i.insn.address == state.addr:
            caps_insn = i.insn
            break

    insn_ops = caps_insn.op_str.split(",")
    insn_ops = [i.replace("[", "").replace("]","").replace("{","").replace("}","") for i in insn_ops]
    read_set = []
    for e in state.history.recent_events:
        if e.type != 'reg':
            continue
        if e.offset in state.tbl_plugin.regs_blacklist:
            continue
        if e.action == 'read' and e.offset not in state.tbl_plugin.regs_blacklist:
            read_set.append(state.arch.register_names[e.offset])
        # if e.action == 'read' and e.offset in state.tbl_plugin.gp_regs:
        #     state.tbl_plugin.dead_regs = state.tbl_plugin.dead_regs - set({state.arch.register_names[e.offset]})
        #if e.action == 'write' and e.offset not in state.tbl_plugin.regs_blacklist:
        
    old_v = state.tbl_plugin.regs[offset]
    new_v = state.registers.load(offset)
    if type(old_v) is angr.state_plugins.sim_action_object.SimActionObject:
        old_v = old_v.ast
    if type(new_v) is angr.state_plugins.sim_action_object.SimActionObject:
        new_v = new_v.ast

    if state.tbl_plugin.vbl:


        for idx, s in enumerate(state.tbl_plugin.shares_vars):
            if s.issubset(new_v.variables):
                if offset not in state.tbl_plugin.regs_blacklist:
                    leaky_reg = state.arch.register_names[offset]
                    lk_expr = new_v
                    vbl_lp_id = state.tbl_plugin.vbl_id_cnt
                    insn_str = insn_str_at_addr(state.addr, state.globals['elf_file'])
                    func_addr = state.callstack.current_function_address
                    func_name = None
                    for symbol in state.project.loader.main_object.symbols:
                        if symbol.rebased_addr == func_addr:
                            func_name = symbol.demangled_name
                            break
                    vbl_l_pt = LeakyPoint(vbl_lp_id,
                                          lk_expr,
                                          state.tbl_plugin.shares[idx],
                                          0x0,
                                          state.addr,
                                          None,
                                          None,
                                          insn_str,
                                          leaky_reg,
                                          read_set,
                                          func_name,
                                          state.globals['addr_cnt'][state.addr],
                                          state.globals['cnt'])
                    state.tbl_plugin.vbl_leaky_points.append(vbl_l_pt)
                    state.tbl_plugin.vbl_cache[new_v.cache_key] = vbl_lp_id
                    state.tbl_plugin.vbl_id_cnt += 1
                    print ("State %s: Potential VBL with ID%d from reg %s" % (
                         hex(state.addr), vbl_lp_id, leaky_reg))
            break
            
    tbl = old_v ^ new_v
    state.tbl_plugin.tbl[offset] = tbl
    share_var_deps = state.tbl_plugin.tbl[offset].variables
    for idx, s in enumerate(state.tbl_plugin.shares_vars):
        if s.issubset(share_var_deps):
            leaky_reg = state.arch.register_names[offset]
            lk_expr = state.tbl_plugin.tbl[offset]

            if offset not in state.tbl_plugin.regs_blacklist:
                if s.issubset(old_v.variables):
                    old_vblp = state.tbl_plugin.vbl_cache[old_v.cache_key]
                else:
                    old_vblp = None
                if s.issubset(new_v.variables):
                    new_vblp = state.tbl_plugin.vbl_cache[new_v.cache_key]
                else:
                    new_vblp = None

                lp_id = state.tbl_plugin.id_cnt

                print ("State {0}: Potential TBL with ID{1} from reg {2} (cnt:{3})".format(hex(state.addr), lp_id, leaky_reg, state.globals['cnt']))
                insn_str = insn_str_at_addr(state.addr, state.globals['elf_file'])
                # add the reg_write that caused
                l_pt = LeakyPoint(lp_id,
                                  lk_expr,
                                  state.tbl_plugin.shares[idx],
                                  state.tbl_plugin.regs_last_write[offset],
                                  state.addr,
                                  old_vblp,
                                  new_vblp,
                                  insn_str,
                                  leaky_reg,
                                  read_set,
                                  None,
                                  state.globals['addr_cnt'][state.addr],
                                  state.globals['cnt'])
                state.tbl_plugin.leaky_points.append(l_pt)
                state.tbl_plugin.id_cnt += 1
        break
    # Update last write location after reporting TBL LP
    state.tbl_plugin.regs_last_write[offset] = state.addr


def z3_solver_with_uf(solver, expr, secrets):
    res_val = solver.BVV(42, 32)
    sec = solver.BVV(0, 8)
    sec_val = solver.BVV(0, 8)
    for s in secrets:
        sec = sec ^ s

    solver.add(expr == res_val)
    for i in range(0, 2**8):
        val = solver.BVV(i, 32)
        log_val = solver.BVV(utils.log[i], 32)
        alog_val = solver.BVV(utils.alog[i], 32)
        solver.add(val.uf_app("log") == log_val)
        solver.add(val.uf_app("alog") == alog_val)
    solver.add(sec == sec_val)
    z3s = solver._solver._owned_solvers.keys()[0]._get_solver()
    return z3s
