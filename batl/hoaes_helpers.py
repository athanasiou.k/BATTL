from angr import SimProcedure
import pdb
import IPython

class RetRand(SimProcedure):
    def run(self, argc, argv):
        next = self.state.globals['rnd_cnt']
        self.state.globals['rnd_cnt'] += 1
        r_bv = self.state.solver.BVS("r%s" % next, 8)
        return r_bv.zero_extend(24)

    
class myInitRand(SimProcedure):
    def run(self, argc, argv):
        rand_array_addr=self.state.globals['rand_array_addr']
        rand_array_bv = self.state.solver.BVS("r{0}".format(0), 8)
        for i in range(1, 200):
            rand_array_bv = rand_array_bv.concat(self.state.solver.BVS("r{0}".format(i), 8))
        self.state.memory.store(rand_array_addr, rand_array_bv, 200)
        return


class KeccakF(SimProcedure):
    def run(self, argc, arv):
        return


#init_key should be 0 at the end of this but it is -16 instead. Look into this.
class GET32(SimProcedure):
    def run(self, argc, argv):
        usart_base = 0x40013800
        usart_sr = usart_base
        usart_dr = usart_base +0x04
        load_base_addr = argc.ast.args[0]
        if load_base_addr == usart_sr:
            bvv = self.state.solver.BVV(0x20, 32)
            return bvv
        elif load_base_addr == usart_dr:
            if self.state.globals['init_rnd'] > 0:
                self.state.globals['init_rnd'] -= 1
                self.state.globals['get_cnt'] += 1
                if self.state.globals['init_rnd']==0 and self.state.globals['get_cnt']==50:
                    print("RNG init done")
                return self.state.memory.load(self.state.regs.r0, 2)
            elif self.state.globals['init_key'] > 0:
                self.state.globals['init_key'] -= 1
                var_name = "k{0}".format(self.state.globals['get_cnt']%50)
            elif self.state.globals['init_txt'] > 0:
                self.state.globals['init_key'] -= 1
                var_name = "p{0}".format(self.state.globals['get_cnt']%(50+16))
            print("Read {0} from USART.".format(var_name))
            bv = self.state.solver.BVS(var_name, 8)#.zero_extend(24)
            self.state.globals['get_cnt'] += 1
            return bv
        else:
            return self.state.memory.load(self.state.regs.r0, 2)
            
            
    
def hook_RetRand_res(state):
    next = state.globals['rnd_cnt']
    state.globals['rnd_cnt'] += 1
    r_bv = state.solver.BVS("r%s" % next, 8)
    state.regs.r0 = r_bv


def hook_RetRand_res_O0(state):
    next = state.globals['rnd_cnt']
    state.globals['rnd_cnt'] += 1
    r_bv = state.solver.BVS("r%s" % next, 8)
    state.regs.r3 = r_bv



## Helper functions for identifying xors that lead to share creation.

## Used as breakpoint to detect xors after the inputs are read.
def is_xor(state):
    if (state.inspect.reg_write_expr.op == '__xor__' and
    state.inspect.reg_write_offset not in state.globals['regs_blacklist']):
        return True
    else:
        return False
    
## Counts the number of xors to i) mask key and inputs, and ii) create the
## respective shares, and replaces the type ii) xors with share variables
## (ci's).
def cnt_xors(state):
    xors = state.globals['xor_cnt']
    state.globals['xor_cnt'] += 1
    print("xor at",state, state.inspect.reg_write_expr)
    if xors < state.globals['key_input_mask_xors']:
        return
    # The following are tested with O2
    if state.globals['llvm_opt']:
        if xors < state.globals['key_input_mask_xors'] + state.globals['share_creation_xors']//2:
            p = None
            k = None
            for vs in state.inspect.reg_write_expr.variables:
                if 'p' in vs:
                    p = vs.split("_")[0][1:]
                elif 'k' in vs:
                    k = vs.split("_")[0][1:]
            if p !=k:
                print("Error, must throw exception here")
            ci = state.solver.BVS("c{0}_{1}".format(p, 0), 8)
            state.globals['share_order_llvm_opt'].append(p)
            key_idx=int(p)
        elif xors < (state.globals['key_input_mask_xors'] + state.globals['share_creation_xors']):
#            pdb.set_trace()
            key_idx = int(state.globals['share_order_llvm_opt'].pop(0))
            ci = state.solver.BVS("c{0}_{1}".format(key_idx, 1), 8)
        state.globals['shares_bvs'][key_idx].append(ci)
        reg_offset = state.inspect.reg_write_offset
        print("replacing", state.inspect.reg_write_expr, "with", ci, "at register", state.arch.register_names[reg_offset])
        state.registers.store(reg_offset, ci.zero_extend(24))
    else:
        if xors < (state.globals['key_input_mask_xors'] + state.globals['share_creation_xors']):
            runner = xors - state.globals['key_input_mask_xors']
            key_ptxt_byte_idx = runner % 16
            key_ptxt_byte_share = runner // 16
            ci = state.solver.BVS("c{0}_{1}".format(key_ptxt_byte_idx, key_ptxt_byte_share), 8)
            state.globals['shares_bvs'][key_ptxt_byte_idx].append(ci)
            reg_offset = state.inspect.reg_write_offset
            print("replacing", state.inspect.reg_write_expr, "with", ci, "at register", state.arch.register_names[reg_offset])
            state.registers.store(reg_offset, ci.zero_extend(24))

        #OLD stuff
    # share_xors = state.globals['share_creation_xors']
    # key_txt_xors =state.globals['key_txt_xors']
    # print("xor at",state, state.inspect.reg_write_expr)
    # # Secinv has a single secret/key which is shared. Hence the ['shares_bvs'][0] below.
    # if state.globals['llvm_opt']:
    #     if xors == 1:
    #         ci = state.solver.BVS("c{0}".format(0), 8)
    #         state.globals['shares_bvs'][0].append(ci)
    #         reg_offset = state.inspect.reg_write_offset
    #         print("replacing", state.inspect.reg_write_expr, "with", ci, "at register", state.arch.register_names[reg_offset])
    #         state.registers.store(reg_offset, ci.zero_extend(24))
    #     elif xors == 4:
    #         ci = state.solver.BVS("c{0}".format(1), 8)
    #         state.globals['shares_bvs'][0].append(ci)
    #         reg_offset = state.inspect.reg_write_offset
    #         print("replacing", state.inspect.reg_write_expr, "with", ci, "at register", state.arch.register_names[reg_offset])
    #         state.registers.store(reg_offset, ci.zero_extend(24))
    # elif (xors > share_xors and xors <= share_xors + key_txt_xors):
    #     ci = state.solver.BVS("c{0}".format(xors-share_xors), 8)
    #     state.globals['shares_bvs'][0].append(ci)
    #     reg_offset = state.inspect.reg_write_offset
    #     print("replacing", state.inspect.reg_write_expr, "with", ci, "at register", state.arch.register_names[reg_offset])
    #     state.registers.store(reg_offset, ci.zero_extend(24))
