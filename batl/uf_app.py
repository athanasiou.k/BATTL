from angr.concretization_strategies import SimConcretizationStrategy
from angr.concretization_strategies import SimConcretizationStrategyRange
import pdb

class SimConcretizationStrategyUfApp(SimConcretizationStrategy):
    """
    Concretization strategy that resolves addresses reads from specified symbol
    addresses. These symbols and their addresses are required for
    initialization.

    This strategy is meant to be used in conjuction with generating uf_app ast nodes
    for the supplied symbol addresses.

    The strategy hacks uf_app nodes into memory accesses of .rodata with symbolic
    indices. First it checks if the load using a symbolic index is targeting a
    symbol from .rodata. If it is, then it puts at the base of this symbol the
    the application of the symbol on the symbolic index, and returns the base of
    the symbol as the result of address concretization. Hence, the subsequent
    load will use the uf_app ast node.

    """

    def __init__(self, limit, symbol_addresses, zero_bv, **kwargs): #pylint:disable=redefined-builtin
        super(SimConcretizationStrategyUfApp, self).__init__(**kwargs)
        self._limit = limit
        self._symbol_addresses = symbol_addresses
        self._zero_bv = zero_bv

    def _concretize(self, memory, addr):
        mn,mx = self._range(memory, addr)
        if mn in self._symbol_addresses:
            symbolic_index = None
            for ast in addr.args:
                # Heuristic:
                # Only keep symbolic indices. Iterate over them and remove constants that are symbols.
                if ast.symbolic:
                    symbolic_index = ast
                    for s_addr, value in self._symbol_addresses.items():
                        symbolic_index = symbolic_index.replace(value[1], self._zero_bv)
                    #print(symbolic_index)
                    memory.store(mn, symbolic_index[7:0].uf_app(self._symbol_addresses[mn][0]))
                    return [mn]
