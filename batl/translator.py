import pdb
import astor
import ast
import claripy.ast.base
import random
from functools import reduce
import pickle

'''
The translator iterates claripy's ast expressions (node) and creates a module
where each expression is represented by a class containing:
- a function leaky_expr() that computes the leaky expression,
- a function secrets() that returns a list of names of the secret variables
- a function randoms() that returns a list of names of the random variables
- a set of the secret variables.
'''


ast_binop = {
    '__add__': ast.Add(),
    '__sub__': ast.Sub(),
    '__mul__': ast.Mult(),
    '__or__': ast.BitOr(),
    '__and__': ast.BitAnd(),
    '__xor__': ast.BitXor(),
    '__lshift__': ast.LShift(),
    '__rshift__': ast.RShift(),
    'LShR': ast.RShift()
}

ast_cmpop = {
    '__eq__': ast.Eq(),
    '__ne__': ast.NotEq(),
    '__ge__': ast.GtE(),
    '__le__': ast.LtE(),
    '__gt__': ast.Gt(),
    '__lt__': ast.Lt(),
    'ULE': ast.LtE()
}


def py_ast_cmpop(node):
    left = py_ast_expr(node.args[0])
    right = py_ast_expr(node.args[1])
    return ast.Compare(left, [ast_cmpop[node.op]], [right])


def py_ast_binop(node):
    if len(node.args) == 1:
        return py_ast_expr(node.args[0])
    left = py_ast_expr(node.args[0])
    right = py_ast_expr(node.args[1])
    op = ast_binop[node.op]
    temp = ast.BinOp(left, op, right)
    tmod = ast.BinOp(temp, ast.BitAnd(), ast.Num(pow(2, node.length)-1))
    if node.op in claripy.operations.commutative_operations:
        for i in range(2, len(node.args)):
            left = tmod
            right = py_ast_expr(node.args[i])
            temp = ast.BinOp(left, op, right)
            tmod = ast.BinOp(temp, ast.BitAnd(),
                             ast.Num(pow(2, node.length)-1))
    return tmod


def py_ast_expr(node):
    if isinstance(node, claripy.ast.Base):
        if node.op == 'BVV':
            return ast.Num(node.args[0])
        elif node.op == 'BVS':
            name = ast.Name('self', ast.Load())
            return ast.Attribute(name, node.args[0], ast.Load())
        elif node.op in ast_binop:
            return py_ast_binop(node)
        elif node.op in ast_cmpop:
            return py_ast_cmpop(node)
        elif node.op == 'ZeroExt':
            return py_ast_expr(node.args[1])
        elif node.op == 'Concat':
            left = py_ast_expr(node.args[0])
            right = py_ast_expr(node.args[1])
            shift_length = ast.Num(node.args[1].length)
            temp = ast.BinOp(left, ast.LShift(), shift_length)
            res = ast.BinOp(temp, ast.BitXor(), right)
            return res
        elif node.op == '__invert__':
            expr = py_ast_expr(node.args[0])
            invr = ast.UnaryOp(ast.Invert(), expr)
            return ast.BinOp(invr, ast.BitAnd(),
                             ast.Num(pow(2, node.length)-1))
        elif node.op == 'Extract':
            expr = py_ast_expr(node.args[2])
            left_most = node.args[0]
            right_most = node.args[1]
            extraction_length = left_most - right_most + 1
            mask = ast.Num(int('1'*extraction_length, 2) << right_most)
            idx = ast.Num(right_most)
            temp = ast.BinOp(mask, ast.BitAnd(), expr)
            res = ast.BinOp(temp, ast.RShift(), idx)
            return res
        elif node.op == 'If':
            test = py_ast_expr(node.args[0])
            body = py_ast_expr(node.args[1])
            orel = py_ast_expr(node.args[2])
            return ast.IfExp(test, body, orel)
        elif node.op == 'uf_app':
            lu_name = ast.Name("tables", ast.Load())
            attr = ast.Attribute(lu_name, node.args[0], ast.Load())
            index = py_ast_expr(node.args[1])
            sub_idx = ast.Index(index)
            return ast.Subscript(attr, sub_idx, ast.Load())
        elif node.op == 'Reverse':
            args = list(node.args[0].args)
            args.reverse()
            rev_claripy = args[0]
            for i in range(1, len(args)):
                rev_claripy = args[i].concat(rev_claripy)
            return py_ast_expr(rev_claripy)
        else:
            print (node.op, "not implemented")


def py_ast_lkg_func(node):
    arg = ast.arg(annotation=None, arg='self')
    ast_args = ast.arguments(args=[arg], defaults=[], vararg=None, kwarg=None)
    leaky_expr = py_ast_expr(node)
    return_stmt = ast.Return(leaky_expr)
    fcn = ast.FunctionDef(name="leaky_expr", args=ast_args,
                          body=[return_stmt], decorator_list=[])
    return fcn


def py_ast_var_list_func(variables, name):
    var_list = list(map((lambda x: ast.Str(x)), list(variables)))
    list_expr = ast.List(var_list, ast.Load())
    return_stmt = ast.Return(list_expr)
    ast_args = ast.arguments(args=[ast.arg(arg='self',
                                           annotation=None)],
                             defaults=[],
                             annotation=None,
                             vararg=None, kwarg=None)
    fcn = ast.FunctionDef(name=name, args=ast_args,
                          body=[return_stmt], decorator_list=[])
    return fcn


# Last secret share k_d gets substituted by k + k_0 + ... + k_{d-1}
def py_ast_cls(lp_id, lp):
    key = claripy.BVS('c', lp.shares[0].size())
    key_expr = key
    print(lp_id)
    for ci in lp.shares[:-1]:
        key_expr ^= ci
    lk_expr_sub = lp.lk_expr.replace(lp.shares[-1], key_expr, uf_app=True)
    lk_fnc = py_ast_lkg_func(lk_expr_sub)
    sec_fnc = py_ast_var_list_func(list(key.variables), "secrets")
    # NOTE: it is unclear why angr doesnt remove shares[-1] from the variable
    # set.
    shr_vars = lp.shares[0].variables
    for i in range(1, len(lp.shares)):
        shr_vars = shr_vars.union(lp.shares[i].variables)
    # This might break stuff.
    # shr_vars = reduce(lambda a, b: a.variables.union(b.variables), lp.shares)
    rnd_vars = lk_expr_sub.variables - key.variables - shr_vars
    rnd_fnc = py_ast_var_list_func(list(rnd_vars), "randoms")
    shr_vars = lp.shares[0].variables
    for i in lp.shares[1:-1]:
        shr_vars = shr_vars.union(i.variables)
    shr_fun = py_ast_var_list_func(list(shr_vars), "shares")
    addr_class_var = ast.Assign([ast.Name("addr", ast.Store())],
                                ast.Str(hex(lp.new_write_addr)))
    reg_class_var = ast.Assign([ast.Name("reg", ast.Store())],
                               ast.Str(lp.reg_name))
    id_class_var = ast.Assign([ast.Name("id", ast.Store())],
                              ast.Num(lp.lp_id))
    body = [id_class_var, addr_class_var, reg_class_var, lk_fnc, sec_fnc, shr_fun, rnd_fnc]
    cls = ast.ClassDef(name="LP"+str(lp_id),
                       bases=[ast.Name(id='object', ctx=ast.Load())], body=body,
                       decorator_list=[])
    return cls


def py_ast_mdl(leaky_points, file_name=None):
    body = [ast.Import(names=[ast.alias('eval.tables', 'tables')]),
            ast.Import(names=[ast.alias('sys', None)]),
            ast.Import(names=[ast.alias('inspect', None)])]
    for i, lp in enumerate(leaky_points):
        if i > 90:
            print("Stopped printing at LP{0} to avoid expression blowup".format(i))
            break
        print("translating LP{0}.".format(i))
        cls = py_ast_cls(i, lp)
        body.append(cls)
    epilogue = ast.parse('''
def leaky_points():
    res = []
    for name, obj in inspect.getmembers(sys.modules[__name__]):
        if inspect.isclass(obj):
                res.append(obj())
    return res''')
    body.append(epilogue)
    mdl = ast.Module(body)
    if file_name is None:
        return mdl
    file = open(file_name, "w")
    file.write(astor.to_source(mdl))
    #file.write(visitors.to_source(mdl))
    file.close()
    return mdl


def py_ast_expr_cls(lk_expr, shares, length):
    key = claripy.BVS('c', shares[0].size())
    key_expr = key
    #print(lp_id)
    for ci in shares[:-1]:
        key_expr ^= ci
    lk_expr_sub = lk_expr.replace(shares[-1], key_expr, uf_app=True)
    lk_fnc = py_ast_lkg_func(lk_expr_sub)
    sec_fnc = py_ast_var_list_func(list(key.variables), "secrets")
    # NOTE: it is unclear why angr doesnt remove shares[-1] from the variable
    # set.
    shr_vars = shares[0].variables
    for i in range(1, len(shares)):
        shr_vars = shr_vars.union(shares[i].variables)
    # This might break stuff.
    # shr_vars = reduce(lambda a, b: a.variables.union(b.variables), lp.shares)
    rnd_vars = lk_expr_sub.variables - key.variables - shr_vars
    rnd_fnc = py_ast_var_list_func(list(rnd_vars), "randoms")
    shr_vars = shares[0].variables
    for i in shares[1:-1]:
        shr_vars = shr_vars.union(i.variables)
    shr_fun = py_ast_var_list_func(list(shr_vars), "shares")
    id_class_var = ast.Assign([ast.Name("id", ast.Store())],
                              ast.Num(0))
    lng_class_var = ast.Assign([ast.Name("length", ast.Store())],
                               ast.Num(length))
    body = [lng_class_var, id_class_var,lk_fnc, sec_fnc, shr_fun, rnd_fnc]
    cls = ast.ClassDef(name="LP",
                       bases=[ast.Name(id='object', ctx=ast.Load())], body=body,
                       decorator_list=[])
    return cls

def py_ast_expr_module(lk_expr, shares, mdl_name, length):
    body = [ast.Import(names=[ast.alias('eval.tables', 'tables')]),
            ast.Import(names=[ast.alias('sys', None)]),
            ast.Import(names=[ast.alias('inspect', None)])]
    cls = py_ast_expr_cls(lk_expr, shares, length)
    body.append(cls)
    epilogue = ast.parse('''
def leaky_points():
    res = []
    for name, obj in inspect.getmembers(sys.modules[__name__]):
        if inspect.isclass(obj):
                res.append(obj())
    return res''')
    body.append(epilogue)
    mdl = ast.Module(body)
    file = open(mdl_name, "w")
    file.write(astor.to_source(mdl))
    #file.write(visitors.to_source(mdl))
    file.close()

    return mdl

def executable_leaky_points(plp_file, elp_file):
    with open(plp_file, 'rb') as f:
        tbl_leaky_points, vbl_leaky_points = pickle.load(f)

    if "vbl" in elp_file:
        py_ast_mdl(vbl_leaky_points, elp_file)
    else:
        py_ast_mdl(tbl_leaky_points, elp_file)

# I need to rethink the design of this to actually test what we are currently
# producing. In test_expr, m is a module with a single class LP.
def test_expr(m, expr, tcase=None):
    expr_vars = set()
    for leaf in expr.recursive_leaf_asts:
        if leaf.op == "BVS":
            expr_vars.add(leaf)
    if len(list(expr_vars)) != len(list(expr.variables)):
        print ("fishy")
    num_vars = len(expr_vars)
    rnd_input = [random.randint(0, 255) for i in xrange(num_vars)]
    var_val_map = {}
    str_val_map = {}
    if tcase is None:
        tcase = rnd_input
    print ("Testing with:", tcase)
    for var in expr_vars:
        conc_inp = tcase.pop()
        var_val_map[var] = claripy.BVV(conc_inp, var.length)
        str_val_map[var.args[0]] = conc_inp
    s = claripy.Solver()
    res = claripy.BVS('res', expr.length)
    s.add(res == expr)
    for var in expr_vars:
        s.add(var == var_val_map[var])
    print (s.eval(res, 2))
    exec(compile(ast.fix_missing_locations(m), '', 'exec'), globals())
    cls_name = m.body[3].name
    cls = globals()[cls_name]()
    for var_str in str_val_map:
        setattr(cls, var_str, str_val_map[var_str])
    print (cls.leaky_expr())
    return cls
