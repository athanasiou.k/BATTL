from angr import SimProcedure
import pdb

class RetRand(SimProcedure):
    def run(self, argc, argv):
        next = self.state.globals['rnd_cnt']
        self.state.globals['rnd_cnt'] += 1
        r_bv = self.state.solver.BVS("r%s" % next, 8)
        return r_bv.zero_extend(24)


class KeccakF(SimProcedure):
    def run(self, argc, arv):
        return

class GET32(SimProcedure):
    def run(self, argc, argv):
        usart_base = 0x40013800
        usart_sr = usart_base
        usart_dr = usart_base +0x04
        load_base_addr = argc.ast.args[0]
        if load_base_addr == usart_sr:
            bvv = self.state.solver.BVV(0x20, 32)
            return bvv
        elif load_base_addr == usart_dr:
            if self.state.globals['init_rnd'] > 0:
                self.state.globals['init_rnd'] -= 1
                self.state.globals['get_cnt'] += 1
                if self.state.globals['init_rnd']==0 and self.state.globals['get_cnt']==50:
                    print("RNG init done")
                return self.state.memory.load(self.state.regs.r0, 2)
            elif self.state.globals['init_key'] > 0:
                self.state.globals['init_key'] -= 1
                var_name = "k{0}".format(self.state.globals['get_cnt']%50)
            elif self.state.globals['init_txt'] > 0:
                self.state.globals['init_key'] -= 1
                var_name = "p{0}".format(self.state.globals['get_cnt']%(50+2))
            print("Read {0} from USART.".format(var_name))
            bv = self.state.solver.BVS(var_name, 8)#.zero_extend(24)
            self.state.globals['get_cnt'] += 1
            return bv
        else:
            return self.state.memory.load(self.state.regs.r0, 2)
            
            
    
def hook_RetRand_res(state):
    next = state.globals['rnd_cnt']
    state.globals['rnd_cnt'] += 1
    r_bv = state.solver.BVS("r%s" % next, 8)
    state.regs.r0 = r_bv


def hook_RetRand_res_O0(state):
    next = state.globals['rnd_cnt']
    state.globals['rnd_cnt'] += 1
    r_bv = state.solver.BVS("r%s" % next, 8)
    state.regs.r3 = r_bv



## Helper functions for identifying xors that lead to share creation.

## Used as breakpoint to detect xors after the inputs are read.
def is_xor(state):
    if (state.inspect.reg_write_expr.op == '__xor__' and
    state.inspect.reg_write_offset not in state.globals['regs_blacklist']):
        return True
    else:
        return False
    
## Counts the number of xors to create the respective shares, and replace the
## xors with share variables (ci's).
def cnt_xors(state):
    state.globals['xor_cnt'] += 1
    xors = state.globals['xor_cnt']
    share_xors = state.globals['share_creation_xors']
    key_txt_xors =state.globals['key_txt_xors']
    print("xor at",state, state.inspect.reg_write_expr)
    # Secinv has a single secret/key which is shared. Hence the ['shares_bvs'][0] below.
    if state.globals['llvm_opt']:
        if xors == 1:
            ci = state.solver.BVS("c{0}".format(0), 8)
            state.globals['shares_bvs'][0].append(ci)
            reg_offset = state.inspect.reg_write_offset
            print("replacing", state.inspect.reg_write_expr, "with", ci, "at register", state.arch.register_names[reg_offset])
            state.registers.store(reg_offset, ci.zero_extend(24))
        elif xors == 4:
            ci = state.solver.BVS("c{0}".format(1), 8)
            state.globals['shares_bvs'][0].append(ci)
            reg_offset = state.inspect.reg_write_offset
            print("replacing", state.inspect.reg_write_expr, "with", ci, "at register", state.arch.register_names[reg_offset])
            state.registers.store(reg_offset, ci.zero_extend(24))
    elif (xors > share_xors and xors <= share_xors + key_txt_xors):
        ci = state.solver.BVS("c{0}".format(xors-share_xors), 8)
        state.globals['shares_bvs'][0].append(ci)
        reg_offset = state.inspect.reg_write_offset
        print("replacing", state.inspect.reg_write_expr, "with", ci, "at register", state.arch.register_names[reg_offset])
        state.registers.store(reg_offset, ci.zero_extend(24))
