from angr import SimProcedure
import pdb

class RetRand(SimProcedure):
    def run(self, argc, argv):
        next = self.state.globals['rnd_cnt']
        if next < 4:
            r_bv = self.state.solver.BVS("c0_%s" % next, 32)
            self.state.globals['shares_bvs'].append([r_bv])
        else:
            r_bv = self.state.solver.BVS("r%s" % next, 32)
        self.state.globals['rnd_cnt'] += 1
        return r_bv


class KeccakF(SimProcedure):
    def run(self, argc, arv):
        return

class GET32(SimProcedure):
    def run(self, argc, argv):
        usart_base = 0x40013800
        usart_sr = usart_base
        usart_dr = usart_base +0x04
        load_base_addr = argc.ast.args[0]
        if load_base_addr == usart_sr:
            bvv = self.state.solver.BVV(0x20, 32)
            return bvv
        elif load_base_addr == usart_dr:
            if self.state.globals['init_rnd'] > 0:
                self.state.globals['init_rnd'] -= 1
                self.state.globals['get_cnt'] += 1
                if self.state.globals['init_rnd']==0 and self.state.globals['get_cnt']==50:
                    print("RNG init done")
                return self.state.memory.load(self.state.regs.r0, 2)
            elif self.state.globals['init_key'] > 0:
                self.state.globals['init_key'] -= 1
                var_name = "k{0}".format(self.state.globals['get_cnt']%50)
            elif self.state.globals['init_txt'] > 0:
                self.state.globals['init_txt'] -= 1
                var_name = "p{0}".format(self.state.globals['get_cnt']%(50+16))
            print("Read {0} from USART.".format(var_name))
            bv = self.state.solver.BVS(var_name, 8)#.zero_extend(24)
            self.state.globals['get_cnt'] += 1
            return bv
        else:
            return self.state.memory.load(self.state.regs.r0, 2)
            
            
    
def hook_RetRand_res(state):
    next = state.globals['rnd_cnt']
    state.globals['rnd_cnt'] += 1
    r_bv = state.solver.BVS("r%s" % next, 8)
    state.regs.r0 = r_bv


def hook_RetRand_res_O0(state):
    next = state.globals['rnd_cnt']
    state.globals['rnd_cnt'] += 1
    r_bv = state.solver.BVS("r%s" % next, 8)
    state.regs.r3 = r_bv



## Helper functions for identifying xors that lead to share creation.

## Used as breakpoint to detect xors after the inputs are read.
def is_xor(state):
    if (state.inspect.reg_write_expr.op == '__xor__' and
    state.inspect.reg_write_offset not in state.globals['regs_blacklist'] and
    (state.inspect.mem_read_address is None)):
        #pdb.set_trace()
        return True
    else:
        return False


def cnt_ptxt_xors(state):
    xors = state.globals['ptxt_xors']
    if xors < 2 :
        p = state.solver.BVS("p%s" % xors, 32)
        reg_offset = state.inspect.reg_write_offset
        print("replacing with", p, "at register", state.arch.register_names[reg_offset])
        state.registers.store(reg_offset, p)
    state.globals['ptxt_xors'] += 1
    
## Counts the number of xors to create the respective shares, and replace the
## xors with share variables (ci's).
def cnt_xors(state):
    xors = state.globals['xor_cnt']
    if xors < 4 :
        c1 = state.solver.BVS("c1_%s" % xors, 32)
        state.globals['shares_bvs'][xors].append(c1)
        reg_offset = state.inspect.reg_write_offset
        print("replacing with", c1, "at register", state.arch.register_names[reg_offset])
        state.registers.store(reg_offset, c1)
    elif xors < 124 and (xors - 3) % 3 == 0:
        var_xor_num = 4+(xors-4)//3
        c0 = state.solver.BVS("c0_{0}".format(var_xor_num), 32)
        state.globals['shares_bvs'].append([c0])
        reg_offset = state.inspect.reg_write_offset
        print("replacing with", c0, "at register", state.arch.register_names[reg_offset])
        state.registers.store(reg_offset, c0)
    elif xors >= 124 and xors < 284 and ((xors - 123) % 4 == 0):
        var_xor_num = 4+(xors-124)//4
        c1 = state.solver.BVS("c1_{0}".format(var_xor_num), 32)
        state.globals['shares_bvs'][var_xor_num].append(c1)
        reg_offset = state.inspect.reg_write_offset
        print("replacing with", c1, "at register", state.arch.register_names[reg_offset])
        state.registers.store(reg_offset, c1)
    state.globals['xor_cnt'] += 1
    # share_xors = state.globals['share_creation_xors']
    # key_txt_xors =state.globals['key_txt_xors']
    # print("xor at",state, state.inspect.reg_write_expr)
    # if state.globals['llvm_opt']:
    #     if xors == 1:
    #         ci = state.solver.BVS("c{0}".format(0), 8)
    #         state.globals['shares_bvs'].append(ci)
    #         reg_offset = state.inspect.reg_write_offset
    #         print("replacing", state.inspect.reg_write_expr, "with", ci, "at register", state.arch.register_names[reg_offset])
    #         state.registers.store(reg_offset, ci.zero_extend(24))
    #     elif xors == 4:
    #         ci = state.solver.BVS("c{0}".format(1), 8)
    #         state.globals['shares_bvs'].append(ci)
    #         reg_offset = state.inspect.reg_write_offset
    #         print("replacing", state.inspect.reg_write_expr, "with", ci, "at register", state.arch.register_names[reg_offset])
    #         state.registers.store(reg_offset, ci.zero_extend(24))
    # elif (xors > share_xors and xors <= share_xors + key_txt_xors):
    #     ci = state.solver.BVS("c{0}".format(xors-share_xors), 8)
    #     state.globals['shares_bvs'].append(ci)
    #     reg_offset = state.inspect.reg_write_offset
    #     print("replacing", state.inspect.reg_write_expr, "with", ci, "at register", state.arch.register_names[reg_offset])
    #     state.registers.store(reg_offset, ci.zero_extend(24))

## Counts the number of xors to create the respective shares, and replace the
## xors with share variables (ci's).
def cnt_strs(state):
    #pdb.set_trace()
    strs = state.globals['str_cnt']
    if strs < 4:
        print("writing {0} at location {1}",state.inspect.mem_write_expr, state.inspect.mem_write_address)
        k0 = state.solver.BVS("k0_{0}".format(strs), 32)
        mem_offset = state.inspect.mem_write_address
        state.globals['str_addresses'].append(mem_offset)
        state.globals['str_exprs'].append(k0)
    elif strs < 8:
        print("writing {0} at location {1}",state.inspect.mem_write_expr, state.inspect.mem_write_address)
        k1 = state.solver.BVS("k1_{0}".format(strs%4), 32)
        mem_offset = state.inspect.mem_write_address
        state.globals['str_addresses'].append(mem_offset)
        state.globals['str_exprs'].append(k1)
    elif strs < 48:
        print("writing {0} at location {1}",state.inspect.mem_write_expr, state.inspect.mem_write_address)
        k0 = state.solver.BVS("k0_{0}".format(strs-4), 32)
        mem_offset = state.inspect.mem_write_address
        state.globals['str_addresses'].append(mem_offset)
        state.globals['str_exprs'].append(k0)
    elif strs < 88:
        print("writing {0} at location {1}",state.inspec.mem_write_expr, state.inspect.mem_write_address)
        k1 = state.solver.BVS("k1_{0}".format(strs-44), 32)
        mem_offset = state.inspect.mem_write_address
        state.globals['str_addresses'].append(mem_offset)
        state.globals['str_exprs'].append(k1)
    state.globals['str_cnt'] += 1

    # share_xors = state.globals['share_creation_xors']
    # key_txt_xors =state.globals['key_txt_xors']
    # print("xor at",state, state.inspect.reg_write_expr)
    # if state.globals['llvm_opt']:
    #     if xors == 1:
    #         ci = state.solver.BVS("c{0}".format(0), 8)
    #         state.globals['shares_bvs'].append(ci)
    #         reg_offset = state.inspect.reg_write_offset
    #         print("replacing", state.inspect.reg_write_expr, "with", ci, "at register", state.arch.register_names[reg_offset])
    #         state.registers.store(reg_offset, ci.zero_extend(24))
    #     elif xors == 4:
    #         ci = state.solver.BVS("c{0}".format(1), 8)
    #         state.globals['shares_bvs'].append(ci)
    #         reg_offset = state.inspect.reg_write_offset
    #         print("replacing", state.inspect.reg_write_expr, "with", ci, "at register", state.arch.register_names[reg_offset])
    #         state.registers.store(reg_offset, ci.zero_extend(24))
    # elif (xors > share_xors and xors <= share_xors + key_txt_xors):
    #     ci = state.solver.BVS("c{0}".format(xors-share_xors), 8)
    #     state.globals['shares_bvs'].append(ci)
    #     reg_offset = state.inspect.reg_write_offset
    #     print("replacing", state.inspect.reg_write_expr, "with", ci, "at register", state.arch.register_names[reg_offset])
    #     state.registers.store(reg_offset, ci.zero_extend(24))
