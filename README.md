# BATTL
A Binary Analysis Tool for Transition-based Leakage.

- Module `batl.*` implements the SymEx component.
- Module `batl.eval` contains functionality related to ILA computation.
- Module `batl.eval.tree_classification.leak_rules.py` implements the Rule Based System.

## Use cases
We have evaluated BATTL on first-order secure implementations of Secure Inversion and AES:
- https://gitlab.com/athanasiou.k/secinv
- https://gitlab.com/athanasiou.k/hoaes

In the above repositories, see the files `*_driver.py` on how BATTL can be used as a library for detection and repair of TBL.

