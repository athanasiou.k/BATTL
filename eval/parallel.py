import time
import os
import multiprocessing as mp
import importlib
import numpy as np
import sys


class BreakNestedLoop(Exception): pass

input_lp_id = int(sys.argv[1])
study_name = "SMFO"
elp_module = "studies.%s.executable_leaky_points" % study_name
elp = importlib.import_module(elp_module)
study_dir = os.path.dirname(elp.__file__)
res_file_name = os.path.join(study_dir, "uni-res.tx")
lps = elp.leaky_points()

for lp in lps:
    lp_id = int(lp.__class__.__name__[2:])
    if lp_id == input_lp_id:
        break

print("lp_id:",lp_id)

input_word_size = 8
output_word_size = 32
input_int_size = 2**input_word_size


secret = lp.secrets()[0]
randoms = lp.randoms()
shares = lp.shares()
var_num = len(randoms)+len(shares)+1
num_inputs = input_int_size ** var_num


def hamming_weight(x):
    return bin(x).count("1")

def count(bounds):
    u_table = np.zeros((input_int_size, output_word_size+1), dtype=np.int)
    for i in range(*bounds):
        secret_val = i % input_int_size
        setattr(lp, secret, secret_val)
        for share_idx, share in enumerate(shares):
            share_val = (i // (input_int_size**(share_idx+1))) % input_int_size
            setattr(lp, share, share_val)
        for random_idx, random in enumerate(randoms):
            random_val = (i // (input_int_size**(len(shares)+random_idx+1))) % input_int_size
            setattr(lp, random, random_val)
        lk_expr_res=lp.leaky_expr()
        hw_res = hamming_weight(lk_expr_res)
        u_table[secret_val, hw_res] += 1
    print("done:", bounds)
    return u_table



def uniformity_check(u_t):
    try:
        for i in range(output_word_size+1):
            non_zero = None
            non_zero_idx = None
            for j in range(input_int_size):
                if u_t[j, i] != 0:
                    non_zero = u_t[j, i]
                    non_zero_idx = j
                    break
            if non_zero is None:
                    continue
            for j in range(non_zero_idx+1, input_int_size):
                if u_t[j, i] != 0 and u_t[j, i] != non_zero:
                    raise BreakNestedLoop
        res_string = "LP{0} is uniform".format(lp_id)
    except BreakNestedLoop:
        res_string = "LP{0} is non-uniform".format(lp_id)

    print(res_string)

    # with open(res_file_name, "a") as res_file:
    #     res_file.write(res_string)
    # lp_file = os.path.join(study_dir, "LP{0}_par-utable.csv".format(lp_id))
    # np.savetxt(lp_file, u_t, delimiter=",", fmt='%i')

def ila(u_t):
    expect_of_g = np.zeros(input_int_size)
    for i in range(input_int_size):
        for j in range(1, output_word_size + 1):
            expect_of_g[i] = expect_of_g[i] + j*u_t[i, j]/input_int_size**(len(randoms)+len(shares))
    confusion_vector = np.zeros(input_int_size)
    kc = 10
    for k in range(input_int_size):
        Each_case = np.zeros(input_int_size)
        for p in range(input_int_size):
            Each_case[p] = (expect_of_g[k ^ p] - expect_of_g[kc ^ p])**2
        confusion_vector[k] = Each_case.mean()

    ila = sum(confusion_vector)/(input_int_size-1)
    res_string = "LP{0} has ila {1}\n".format(lp_id, ila)
    print(res_string)
    # with open(res_file_name, "a") as res_file:
    #     res_file.write(res_string)

    
if __name__ ==  '__main__':

    
    cpus = os.cpu_count()
    
    chunk = (num_inputs) // cpus
    bounds = [(i*chunk, i*chunk+chunk) for i in range(cpus-1)]
    bounds.append(((cpus-1)*chunk, num_inputs))
    print("num_inputs:", num_inputs)
    print("num_vars:", var_num)
    print("chunk_size", chunk)
    print("cpus", cpus)
    # if (num_inputs) % cpus != 0:
    #     bounds.append((cpus*chunk, num_inputs))
    print(bounds)
    pool = mp.Pool()
    s_time = time.time()
    res = pool.map(count, bounds)
    e_time = time.time()
    print(e_time - s_time)

    u_t = sum(res)
    print("done with u_t")
    uniformity_check(u_t)
    #ila(u_t)
    ## TODO: Must be adjusted to take as input plp files.

    
