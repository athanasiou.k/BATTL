import numpy as np
import importlib.util
import inspect
import pickle
import batl.utils
import pdb
import time
import os
import json

input_word_size = 8
input_word_range = 2**input_word_size
output_word_size = 32
sample_size = 10000 #65536 # 200000

def hamming_distance(x, y):
    return bin(x ^ y).count("1")


def hamming_weight(x):
    return bin(x).count("1")


def hamming_weight_n_bits(x, n):
    mask = x & (pow(2, n) - 1)
    return bin(mask).count("1")


def sampled_hw_table(lp):
    num_rands=len(lp.randoms())
    num_shares=len(lp.shares())
    value_range = list(range(0, input_word_range))

    hw_table = np.zeros((input_word_range, output_word_size+1))
    sample_space = np.zeros((sample_size, num_rands+num_shares))

    for j in range(0, sample_size):
        sample_space[j, :] = np.random.choice(value_range, size=num_shares+num_rands, replace=True)
        
    for x in range(0, input_word_range):
        setattr(lp, lp.secrets()[0], int(x))
        if __debug__: kstr="{:03d}\t".format(x)
        for i in range(0, sample_size):
            if __debug__:
                sstr=""
                rstr=""
                last_share = 0
            for r in range(0, num_shares):
                setattr(lp, lp.shares()[r], int(sample_space[i, r]))
                if __debug__:
                    sstr=sstr+"{:03d}\t".format(int(sample_space[i, r]))
                    last_share ^= int(sample_space[i, r])
                if __debug__: sstr="{:03d}\t".format(last_share^x)+sstr 
            for r in range(0, num_rands):
                setattr(lp, lp.randoms()[r], int(sample_space[i, num_shares+r]))
                if __debug__: rstr=rstr+"{:03d}\t".format(int(sample_space[i, num_shares+r]))
            res = lp.leaky_expr()
            if __debug__:
                resstr= "{:03d}\t".format(res)
                print(rstr+sstr+kstr+resstr)
            hw = hamming_weight_n_bits(res, output_word_size)
            hw_table[x, hw] += 1
    return hw_table

    
def exact_hw_table(lp):
    num_rands=len(lp.randoms())
    num_shares=len(lp.shares())
    hw_table = np.zeros((input_word_range, output_word_size+1))
    for i in range(0, 256**(num_shares+num_rands+1)):
        if __debug__: rstr =""
        # For compatibility with the C ILA impl, rands are least significant than shares
        for j in range(0, num_rands):
            rval = (i // (input_word_range**j)) % input_word_range
            setattr(lp, lp.randoms()[j], rval)
            if __debug__: rstr = rstr + "{:03d}\t".format(rval)
        svals=[]
        key_val = 0
        # The +1 is needed here to take into account for the key substitution that occurs.
        for j in range(num_rands, num_rands+num_shares+1):
            sval = (i // (input_word_range**j)) % input_word_range
            svals.append(sval)
            key_val ^= sval
        if __debug__:
            for j in range(num_shares+1):
                rstr = rstr + "{:03d}\t".format(svals[j])
        # The 1 to +1 is needed here to take into account for the key substitution that removes share 0.
        for j in range(1, num_shares+1):
            setattr(lp, lp.shares()[j-1], svals[j])
        setattr(lp, lp.secrets()[0], key_val)
        res = lp.leaky_expr()
        if __debug__:
            rstr = rstr + "{:03d}\t{:03d}".format(key_val, res)
            print(rstr)
        hw = hamming_weight_n_bits(res, output_word_size)
        hw_table[key_val, hw] += 1    
    return hw_table

## Computes the ILA value of a leaky point object.
def ila(lp):
    if __debug__: print("new")
    total_vars = len(lp.randoms()) + len(lp.shares()) + 1
    exact_ila = len(lp.randoms()) + len(lp.shares()) <= 2
    hw_table = exact_hw_table(lp) if exact_ila else sampled_hw_table(lp)
    if __debug__: np.savetxt("hw_table.out", hw_table, delimiter=',', fmt='%d')

    space_size_wo_key = input_word_range**(total_vars-1) if exact_ila else 2000000
    
    expect_of_g = np.zeros(input_word_range)
    for i in range(0, input_word_range):
        for j in range(1, output_word_size+1):
            expect_of_g[i] = expect_of_g[i] + j*hw_table[i, j]/space_size_wo_key

    # Calculating confusion coefficient for different kg
    confusion_vector = np.zeros(input_word_range)
    kc = 10
    for k in range(0, input_word_range):
        Each_case = np.zeros((input_word_range))
        for p in range(0, input_word_range):
            Each_case[p] = (expect_of_g[k ^ p] - expect_of_g[kc ^ p])**2
        confusion_vector[k] = Each_case.mean()
        
    return sum(confusion_vector)/(input_word_range-1)


## Computes the ILA value of a leaky point object.
## Deprecated
def old_ila(lp):
    #print(lp.__class__.__name__)
    lp_id = int(lp.__class__.__name__[2:])
    print("old")
    #print("ila of {0}".format(lp_id))
    secrets = lp.secrets()
    randoms = lp.randoms()
    shares = lp.shares()
    # Required input : dictionary Parameter : number of variables; number of
    # bytes ; size of sample population
    Parameter = {"num_of_random": len(randoms) + len(shares),
                 "num_of_bits": 8,
                 "size_of_sample": 100000,
                 "bit_of_register": 32}
    V_HW = np.zeros((2**Parameter["num_of_bits"],
                     (Parameter["bit_of_register"]+1)))
    rv_range = list(range(0, 2**Parameter["num_of_bits"]))
    # Constructing sample : 1,2 byte not sampling;>=3 doing sampling
    #pdb.set_trace()
    # if Parameter["num_of_random"] == 1:
    #     sample_par = np.zeros(
    #         (2**(Parameter["num_of_bits"]*Parameter["num_of_random"]),
    #          Parameter["num_of_random"]))
    #     for j in range(0, 2**(Parameter["num_of_bits"])):
    #         sample_par[j, :] = j
    # elif Parameter["num_of_random"] == 2:
    #     sample_par = np.zeros(
    #         (2**(Parameter["num_of_bits"]*Parameter["num_of_random"]),
    #          Parameter["num_of_random"]))
    #     for i in range(0, 2**(Parameter["num_of_bits"])):
    #         for j in range(0, 2**(Parameter["num_of_bits"])):
    #             sample_par[i*(2**(Parameter["num_of_bits"]))+j, :] = [i, j]
    # elif Parameter["num_of_random"] >= 3:
    if True:
        sample_par = np.zeros(
            (Parameter["size_of_sample"],
             Parameter["num_of_random"]))
        for j in range(0, Parameter["size_of_sample"]):
            sample_par[j, :] = np.random.choice(
                rv_range,
                size=Parameter["num_of_random"],
                replace=True)
    # Calculating Expectation over Masks
    for x in range(0, 2**Parameter["num_of_bits"]):
        setattr(lp, secrets[0], int(x))
        if __debug__: kstr="{:03d}\t".format(x)
        for i in range(0, sample_par.shape[0]):
            if __debug__:
                sstr=""
                rstr=""
                last_share = 0
            for r in range(0, len(shares)):
                setattr(lp, shares[r], int(sample_par[i, r]))
                if __debug__:
                    sstr=sstr+"{:03d}\t".format(int(sample_par[i, r]))
                    last_share ^= int(sample_par[i, r])
                if __debug__: sstr="{:03d}\t".format(last_share^x)+sstr 
            for r in range(0, len(randoms)):
                setattr(lp, randoms[r], int(sample_par[i, len(shares)+r]))
                if __debug__: rstr=rstr+"{:03d}\t".format(int(sample_par[i, len(shares)+r]))
            res = lp.leaky_expr()
            if __debug__:
                resstr= "{:03d}\t".format(res)
                print(rstr+sstr+kstr+resstr)
#            ham = hamming_weight(lp.leaky_expr())

            ham = hamming_weight_n_bits(res,
                                        Parameter["bit_of_register"])
            V_HW[x, ham] += 1
    np.savetxt("old_V_HW.out", V_HW, delimiter=',', fmt='%d')

    expect_of_g = np.zeros(2**Parameter["num_of_bits"])
    for i in range(0, 2**Parameter["num_of_bits"]):
        for j in range(1, (Parameter["bit_of_register"]+1)):
            expect_of_g[i] = expect_of_g[i] + j*V_HW[i, j]/sample_par.shape[0]

    # Calculating confusion coefficient for different kg
    confusion_vector = np.zeros(2**Parameter["num_of_bits"])
    kc = 10
    for k in range(0, 2**Parameter["num_of_bits"]):
        Each_case = np.zeros((2**Parameter["num_of_bits"]))
        for p in range(0, 2**Parameter["num_of_bits"]):
            Each_case[p] = (expect_of_g[k ^ p] - expect_of_g[kc ^ p])**2
        confusion_vector[k] = Each_case.mean()
        
    return sum(confusion_vector)/(2**Parameter["num_of_bits"]-1)



    

def ila_of_lp(elp_file, module, plp_file, lp_id, leak_type):
    if leak_type=="VBL":
        elp_file=elp_file+"_vbl.py"
    else:
        elp_file=elp_file+"_tbl.py"
    spec = importlib.util.spec_from_file_location(module+"."+elp_file[:-3], module+"/"+elp_file)
    elp = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(elp)
    print("ILA of", elp, "for LP"+str(lp_id))
    with open(plp_file, 'rb') as f:
        tbl_leaky_points, vbl_leaky_points = pickle.load(f)

    if leak_type=="TBL":
        pik_leaky_points = tbl_leaky_points
    else:
        pik_leaky_points = vbl_leaky_points

    lps = []
    for obj in dir(elp):
        if inspect.isclass(getattr(elp, obj)):
            lps.append(getattr(elp, obj)())

    for lp in lps:
        for pik_lp in pik_leaky_points:
            if pik_lp.lp_id == lp_id and pik_lp.lp_id == lp.id :
                if hasattr(pik_lp, "ila"):
                    print(pik_lp.ila)
                else:
                    ila_val = ila(lp)
                    setattr(pik_lp, "ila", ila_val)
                    print(pik_lp.ila)
                    with open(plp_file, 'wb') as f:
                        pickle.dump((tbl_leaky_points, vbl_leaky_points), f)

def old_ila_of_lp(elp_file, module, plp_file, lp_id, leak_type):
    if leak_type=="VBL":
        elp_file=elp_file+"_vbl.py"
    else:
        elp_file=elp_file+"_tbl.py"
    spec = importlib.util.spec_from_file_location(module+"."+elp_file[:-3], module+"/"+elp_file)
    elp = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(elp)
    print("ILA of", elp, "for LP"+str(lp_id))
    with open(plp_file, 'rb') as f:
        tbl_leaky_points, vbl_leaky_points = pickle.load(f)

    if leak_type=="TBL":
        pik_leaky_points = tbl_leaky_points
    else:
        pik_leaky_points = vbl_leaky_points

    lps = []
    for obj in dir(elp):
        if inspect.isclass(getattr(elp, obj)):
            lps.append(getattr(elp, obj)())

    for lp in lps:
        for pik_lp in pik_leaky_points:
            if pik_lp.lp_id == lp_id and pik_lp.lp_id == lp.id :
                ila_val = old_ila(lp)
                setattr(pik_lp, "ila", ila_val)
                print(pik_lp.ila)
            
        
def ila_values(elp_file, module, plp_file):
    print(elp_file, module, plp_file)
    spec = importlib.util.spec_from_file_location(module+"."+elp_file[:-3], module+"/"+elp_file)
    elp = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(elp)
    print("ILAS of", elp)
    with open(plp_file, 'rb') as f:
        tbl_leaky_points, vbl_leaky_points = pickle.load(f)

    if "tbl" in elp_file:
        pik_leaky_points = tbl_leaky_points
    else:
        pik_leaky_points = vbl_leaky_points
        
    lps = []
    for obj in dir(elp):
        if inspect.isclass(getattr(elp, obj)):
            lps.append(getattr(elp, obj)())
    for lp in lps:
        ila_val = ila(lp)
        lp_id = lp.id
        for pik_lp in pik_leaky_points:
            if pik_lp.lp_id == lp.id:
                setattr(pik_lp, "ila", ila_val)

    if "tbl" in elp_file:
        tbl_leaky_points = pik_leaky_points
    else:
        vbl_leaky_points = pik_leaky_points

    with open(plp_file, 'wb') as f:
        pickle.dump((tbl_leaky_points, vbl_leaky_points), f)
    batl.utils.print_plps(plp_file)


def is_gvbl(ptbl, pvbl, exe_pvbls):
    if hasattr(pvbl, 'leak_class') and (pvbl.leak_class == 'RUD' or pvbl.leak_class == 'SID'):
        return False
    if hasattr(pvbl, 'ila') and (pvbl.ila > 0):
        old_write_ila = pvbl.ila
        print("Reusing ILA(PVBLP{0})={1}".format(pvbl.lp_id, pvbl.ila))
        print("PTBLP{0}'s dep has positive ILA. Skipping.".format(ptbl.lp_id))
        return True
    elif hasattr(pvbl, 'ila') and (pvbl.ila == 0):
        print("Reusing ILA(PVBLP{0})={1}".format(pvbl.lp_id, pvbl.ila))
        return False
    else:
        print("Computing ILA of PVBLP{0} ({1})".format(pvbl.lp_id, pvbl.leak_class))
        start_time=time.time()
        old_write_ila = ila(exe_pvbls[pvbl.lp_id])
        print("%s seconds" % (time.time() - start_time))
        setattr(pvbl, 'ila', old_write_ila)
        if old_write_ila > 0:
            print("PTBLP{0}'s dep has positive ILA. Skipping.".format(ptbl.lp_id))
            return True
        else:
            print("ILA(PVBLP{0})={1}".format(pvbl.lp_id, pvbl.ila))
            return False
                
    
def ila_tblps(elp_vbl_file, elp_tbl_file, module, plp_file):
    vbl_spec = importlib.util.spec_from_file_location(module+"."+elp_vbl_file[:-3], module+"/"+elp_vbl_file)
    vbl_elp = importlib.util.module_from_spec(vbl_spec)
    vbl_spec.loader.exec_module(vbl_elp)

    tbl_spec = importlib.util.spec_from_file_location(module+"."+elp_tbl_file[:-3], module+"/"+elp_tbl_file)
    tbl_elp = importlib.util.module_from_spec(tbl_spec)
    tbl_spec.loader.exec_module(tbl_elp)

    exe_vblps = {}
    for obj in dir(vbl_elp):
        if inspect.isclass(getattr(vbl_elp, obj)):
            exe_vblp = getattr(vbl_elp, obj)()
            exe_vblps[exe_vblp.id] = exe_vblp

    exe_tblps = {}
    for obj in dir(tbl_elp):
        if inspect.isclass(getattr(tbl_elp, obj)):
            exe_tblp = getattr(tbl_elp, obj)()
            exe_tblps[exe_tblp.id] = exe_tblp

    with open(plp_file, 'rb') as f:
        ptblps, pvblps = pickle.load(f)

    for ptblp in ptblps:
        print("-"*10+"PTBLP{0}".format(ptblp.lp_id)+"-"*10)
        if ptblp.old_write_vblp and is_gvbl(ptblp, pvblps[ptblp.old_write_vblp], exe_vblps):
            continue
        if ptblp.new_write_vblp and is_gvbl(ptblp, pvblps[ptblp.new_write_vblp], exe_vblps):
            continue
        if hasattr(ptblp, 'leak_class') and (ptblp.leak_class == 'RUD' or ptblp.leak_class == 'SID'):
            continue
        start_time=time.time()
        print("Computing ILA of PTBLP{0}".format(ptblp.lp_id))
        ptblp_ila = ila(exe_tblps[ptblp.lp_id])
        elapsed_time =time.time() - start_time
        print("%s seconds" % (elapsed_time))
        setattr(ptblp, 'ila', ptblp_ila)
    with open(plp_file, 'wb') as f:
        pickle.dump((ptblps, pvblps), f)
    batl.utils.print_plps(plp_file)



def qemu_ila(leak_id, opt, tc, leak_type, ila_file):
    os.system('make batl-qemu-ila-single ID={0} OPT={1} LEAK={2} TOOLCHAIN={3}'.format(leak_id, opt, leak_type, tc))
    with open(ila_file, "r") as f:
        ila=float(list(f)[-1])
    return ila

def qemu_is_gvbl(ptbl, pvbl, builddir, opt, tc):

    profile_data_f = os.path.join(builddir, "profile_data.txt")

    
    if hasattr(pvbl, 'leak_class') and (pvbl.leak_class == 'RUD' or pvbl.leak_class == 'SID'):
        return False
    if hasattr(pvbl, 'ila') and (pvbl.ila > 0):
        old_write_ila = pvbl.ila
        print("Reusing ILA(PVBLP{0})={1}".format(pvbl.lp_id, pvbl.ila))
        print("PTBLP{0}'s dep has positive ILA. Skipping.".format(ptbl.lp_id))
        return True
    elif hasattr(pvbl, 'ila') and (pvbl.ila == 0):
        print("Reusing ILA(PVBLP{0})={1}".format(pvbl.lp_id, pvbl.ila))
        return False
    else:
        print("Computing ILA of PVBLP{0} ({1})".format(pvbl.lp_id, pvbl.leak_class))
        ila_out_file=os.path.join(builddir,"VBL"+str(pvbl.lp_id)+"/","ila.out")
        start_time=time.time()
        old_write_ila = qemu_ila(pvbl.lp_id, opt, tc, "VBL", ila_out_file)
        elapsed_time = time.time() - start_time
        print("%s seconds" % (elapsed_time))

        with open(profile_data_f) as json_file:
            data = json.load(json_file)
        data["PVBLP{0}_t".format(pvbl.lp_id)] = elapsed_time
        with open(profile_data_f, 'w') as jsonfile:
            json.dump(data, jsonfile, indent=4,sort_keys=True)
        
        setattr(pvbl, 'ila', old_write_ila)
        if old_write_ila > 0:
            print("PTBLP{0}'s dep has positive ILA. Skipping.".format(ptbl.lp_id))
            return True
        else:
            print("ILA(PVBLP{0})={1}".format(pvbl.lp_id, pvbl.ila))
            return False

        
def qemu_ila_tblps(builddir, opt, tc, plp_file):
    with open(plp_file, 'rb') as f:
        ptblps, pvblps = pickle.load(f)

    dirname = os.path.dirname(os.path.abspath(plp_file))
    profile_data_f = os.path.join(dirname, "profile_data.txt")
        
    for ptblp in ptblps:
        print("-"*10+"PTBLP{0}".format(ptblp.lp_id)+"-"*10)
        if ptblp.old_write_vblp and qemu_is_gvbl(ptblp, pvblps[ptblp.old_write_vblp], builddir, opt, tc):
            continue
        if ptblp.new_write_vblp and qemu_is_gvbl(ptblp, pvblps[ptblp.new_write_vblp], builddir, opt, tc):
            continue
        if hasattr(ptblp, 'leak_class') and (ptblp.leak_class == 'RUD' or ptblp.leak_class == 'SID'):
            continue
        ila_out_file=os.path.join(builddir,"TBL"+str(ptblp.lp_id)+"/","ila.out")
        print("Computing ILA of PTBLP{0}".format(ptblp.lp_id))
        start_time=time.time()

        ptblp_ila = qemu_ila(ptblp.lp_id, opt, tc, "TBL", ila_out_file)
        elapsed_time = time.time() - start_time

        with open(profile_data_f) as json_file:
            data = json.load(json_file)
        data["PTBLP{0}_t".format(ptblp.lp_id)] = elapsed_time
        with open(profile_data_f, 'w') as jsonfile:
            json.dump(data, jsonfile, indent=4, sort_keys=True)

        print("%s seconds" % (elapsed_time))
        setattr(ptblp, 'ila', ptblp_ila)
    with open(plp_file, 'wb') as f:
        pickle.dump((ptblps, pvblps), f)
    batl.utils.print_plps(plp_file)


        
