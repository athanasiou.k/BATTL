import pdb
import numpy as np
import importlib.util
import inspect
import pickle
import batl.utils
import time

class BreakNestedLoop(Exception): pass
class EndNestedLoop(Exception): pass

input_word_size = 8
output_word_size = 32
input_int_size = 2**input_word_size


def hamming_weight(x):
    return bin(x).count("1")

def random_byte():
    return np.random.randint(0, 256)

def get_exe_leaks(loaded_exec_leak_module):
    exe_leaks = {}
    for obj in dir(loaded_exec_leak_module):
        if inspect.isclass(getattr(loaded_exec_leak_module, obj)):
            exe_leak = getattr(loaded_exec_leak_module, obj)()
            exe_leaks[exe_leak.id] = exe_leak
    return exe_leaks

def get_loaded_module(exe_leaks_file, module):
    exe_leak_spec = importlib.util.spec_from_file_location(module+"."+exe_leaks_file[:-3],
                                                           module+"/"+exe_leaks_file)
    loaded_exe_leak_module = importlib.util.module_from_spec(exe_leak_spec)
    exe_leak_spec.loader.exec_module(loaded_exe_leak_module)
    return loaded_exe_leak_module

def test_uniform(leak_id, exe_leaks_file, module, eps=0.05, hw_out=3):

    loaded_leaks_file = get_loaded_module(exe_leaks_file, module)
    exe_leaks = get_exe_leaks(loaded_leaks_file)

    leak = exe_leaks[leak_id]
    print("Test uniformity of leakagw with id={0} with output HW={1} and eps={2}".format(leak_id, hw_out, eps))
    secret = leak.secrets()[0]
    randoms = leak.randoms()
    shares = leak.shares()
    var_num = len(randoms)+len(shares)+1
    s_time = time.time()
    sample_set = set()
    num_samples = 0
    true_colls = 0
    bad_samples = 0
    while num_samples < (3200*input_int_size**(0.5))/(eps**2):
        secret_val = random_byte()
        setattr(leak, secret, secret_val)
        sample = (secret_val, )
        for share in shares:
            share_val = random_byte()
            setattr(leak, share, share_val)
            sample = sample + (share_val,)
        for random in randoms:
            random_val = random_byte()
            setattr(leak, random, random_val)
            sample = sample + (random_val,)
        lk_expr_res=leak.leaky_expr()
        hw_res = hamming_weight(lk_expr_res)
        if hw_res == hw_out:
            num_samples += 1
            if sample in sample_set:
                true_colls += 1
            else:
                sample_set.add(sample)
        else:
            bad_samples += 1
    t = ((num_samples)*(num_samples)/2)*(1+3*(eps**2)/4)/input_int_size
    e_time = time.time()
    print("finished in ",(e_time -s_time))
    print(true_colls, t)
    print("required samples:", num_samples)
    print("garbage samples:", bad_samples)


def test_tt(exe_leaks_file, module):
    loaded_leaks_file = get_loaded_module(exe_leaks_file, module)
    exe_leaks = get_exe_leaks(loaded_leaks_file)

    leak = exe_leaks[0]
    var_length = leak.length
    input_int_size = 2**var_length
    print(input_int_size)
    secret = leak.secrets()[0]
    randoms = leak.randoms()
    shares = leak.shares()
    for i in range(input_int_size):
        secret_val = i
        setattr(leak, secret, secret_val)
        distr = dict()
        print(input_int_size**(len(randoms)+len(shares)))
        for j in range(input_int_size**(len(randoms)+len(shares))):
            share_vals = []
            for share_idx, share in enumerate(shares):
                share_val = (j // (input_int_size**(share_idx))) % input_int_size
                share_vals.append(share_val)
                setattr(leak, share, share_val)
            random_vals = []
            for random_idx, random in enumerate(randoms):
                random_val = (j // (input_int_size**(len(shares)+random_idx))) % input_int_size
                random_vals.append(random_val)
                setattr(leak, random, random_val)
            res = leak.leaky_expr()
            print(secret_val, random_vals, share_vals, res)
            if res in distr:
                distr[res] += 1
            else:
                distr[res] = 1
        print(distr)
        for _, v in distr.items():
            if v != input_int_size:
                print("Non Uniform")
        print(40*"-")
            
    


def print_u_table(leak_id, exe_leaks_file, module):
    loaded_leaks_file = get_loaded_module(exe_leaks_file, module)
    exe_leaks = get_exe_leaks(loaded_leaks_file)

    leak = exe_leaks[leak_id]
    print("Computing utable for {0}".format(leak_id))
    secret = leak.secrets()[0]
    randoms = leak.randoms()
    shares = leak.shares()
    var_num = len(randoms)+len(shares)+1
    if var_num > 3:
        print("var_num too large, should use the parallel version instead.")
    u_table = np.zeros((input_int_size, output_word_size+1), dtype=np.int)

    s_time = time.time()
    print(input_int_size**var_num)
    results = []
    for i in range(input_int_size**var_num):
        secret_val = i % input_int_size
        setattr(leak, secret, secret_val)
        for share_idx, share in enumerate(shares):
            share_val = (i // (input_int_size**(share_idx+1))) % input_int_size
            setattr(leak, share, share_val)
        for random_idx, random in enumerate(randoms):
            random_val = (i // (input_int_size**(len(shares)+random_idx+1))) % input_int_size
            setattr(leak, random, random_val)
        lk_expr_res=leak.leaky_expr()
        results.append(lk_expr_res)
        #print(lk_expr_res)
        hw_res = hamming_weight(lk_expr_res)
        u_table[secret_val, hw_res] += 1
    np.savetxt('res.out', np.asarray(results), delimiter=',', fmt="%i")
    np.savetxt('utable.out', u_table, delimiter=',', fmt="%i")
    
def check_uniform(leak_id, exe_leaks_file, module):

    loaded_leaks_file = get_loaded_module(exe_leaks_file, module)
    exe_leaks = get_exe_leaks(loaded_leaks_file)

    leak = exe_leaks[leak_id]
    print("Check uniformity of leakage with id={0} across all HW outputs".format(leak_id))
    secret = leak.secrets()[0]
    randoms = leak.randoms()
    shares = leak.shares()
    var_num = len(randoms)+len(shares)+1
    if var_num > 3:
        print("var_num too large, should use the parallel version instead.")
    u_table = np.zeros((input_int_size, output_word_size+1), dtype=np.int)

    s_time = time.time()
    print(input_int_size**var_num)

    for i in range(input_int_size**var_num):
        secret_val = i % input_int_size
        setattr(leak, secret, secret_val)
        for share_idx, share in enumerate(shares):
            share_val = (i // (input_int_size**(share_idx+1))) % input_int_size
            setattr(leak, share, share_val)
        for random_idx, random in enumerate(randoms):
            random_val = (i // (input_int_size**(len(shares)+random_idx+1))) % input_int_size
            setattr(leak, random, random_val)
        lk_expr_res=leak.leaky_expr()
        hw_res = hamming_weight(lk_expr_res)
        u_table[secret_val, hw_res] += 1
    e_time = time.time()
    try:
        for i in range(output_word_size+1):
            non_zero = None
            non_zero_idx = None
            for j in range(input_int_size):
                if u_table[j, i] != 0:
                    non_zero = u_table[j, i]
                    non_zero_idx = j
                    break
            if non_zero is None:
                    continue
            for j in range(non_zero_idx+1, input_int_size):
                if u_table[j, i] != 0 and u_table[j, i] != non_zero:
                    raise BreakNestedLoop
        print("Leakage with id={0} is uniform".format(leak_id))
    except BreakNestedLoop:
        print("Leakage with id={0} is non-uniform".format(leak_id))
    print("finished in ",(e_time -s_time))
        
