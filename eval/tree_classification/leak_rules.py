import batl.utils
import claripy.ast.base
import pickle
import pdb
import os
import time
import json

leaf_ops = {'BVS', 'BVV'}
bin_ops = {'__xor__', '__or__', '__and__', '__add__', '__mul__', '__ge__', '__eq__', '__sub__', '__lshift__',
           '__rshift__', 'LShR', 'Concat'}
bin_ops_restricted = {'__xor__', '__or__', '__and__', '__add__', '__mul__', '__ge__', '__eq__', '__sub__', '__lshift__',
                      '__rshift__'}
un_ops = {'SignExt','ZeroExt', 'uf_app', 'Extract', '__invert__'}
other_ops = {'If'}
total_ops = bin_ops.union(un_ops).union(leaf_ops).union(other_ops)


# classifies each ast in a list provided by a .plp file.
# provides a summery of the useful information generated in the tree.
# file is the file path pointing to the plp file containing the asts.
# is_tbl is a boolean that determines which set of asts you want from the file.
# True gives the first set False gives the second
def classify(file, is_tbl):
    tbls, vbls = batl.utils.get_lps(file)
    if is_tbl:
        check_masked_trees(tbls)
    else:
        check_masked_trees(vbls)


# provides a more detailed look at what is happening at each node one specific tree
# is_tbl is a boolean that determines which set in the .plp file to look at.
# file is the file path as a string to the .plp file containingt he ast sets.
# id_num is an integer that represents the id of the desired ast.
# raises an exception if the id number is not in the list of leaks.
def classify_detailed(file, is_tbl, id_num):
    tbls, vbls = batl.utils.get_lps(file)
    if is_tbl:
        for tbl in tbls:
            if tbl.lp_id == id_num:
                if hasattr(tbl, 'ila'):
                    print("ILA",  tbl.ila)
                else:
                    print("No ILA calculated")
                print_tree_detailed(tbl)
                return tbl
    else:
        for vbl in vbls:
            if vbl.lp_id == id_num:
                print_tree_detailed(vbl)
                return
    raise Exception('ID: ', id_num, ' not found')

def run_rules(file):

    tbls, vbls = batl.utils.get_lps(file)
    start_time = time.time()
    for vbl in vbls:
        tree, _ = compute_tables(vbl)
#        print("Vbl"+vbl.lp_id+":"+tree.mask_type)
        setattr(vbl, 'leak_class', tree.mask_type)
    for tbl in tbls:
        tree, _ = compute_tables(tbl)
        setattr(tbl, 'leak_class', tree.mask_type)
    leak_rules_times = time.time() - start_time

    with open(file, 'wb') as f:
        pickle.dump((tbls, vbls), f)
        
    dirname = os.path.dirname(os.path.abspath(file))
    profile_data_f = os.path.join(dirname, "profile_data.txt")
    with open(profile_data_f) as json_file:
        data = json.load(json_file)

    data['rules_t'] = leak_rules_times
    with open(profile_data_f, 'w') as jsonfile:
        json.dump(data, jsonfile, indent=4)




# computes data structures for an ast.
# leak is an object that comes from the plp file in the above function.
# returns the top node of the ast and the operations found in the ast.
def compute_tables(leak):
    tree, keys, shares, rands = get_tree_vars(leak)
    return compute_tables_helper(tree, keys, set())


# function determines information about a node from its children.
# fills rand and supp sets for the node.
def get_children_info(node, keys, operations):
    all_vars = set()
    random_total = set()
    perfect_total = set()
    seen_already = set()
    for child in node.args:
        if isinstance(child, claripy.ast.Base):
            compute_tables_helper(child, keys, operations)
            for rand in child.rand:
                if rand in all_vars:
                    seen_already.add(rand)
            all_vars = all_vars.union(child.supp)
            random_total = random_total.union(child.rand)
            perfect_total = perfect_total.union(child.dom)
    randoms = random_total - seen_already
    node.rand = randoms
    node.supp = all_vars
    return perfect_total, seen_already


# helper function for compute tables that recursively runs on the ast leaf to root.
# node is an ast that the function is working on.
# keys is the set of private key bv variables in the ast.
# operation is the operations represented in this ast.
# raises an exception if a node is found with an unknown operation.
def compute_tables_helper(node, keys, operations):
    if node.op in total_ops:
        if not hasattr(node, 'mask_type'):
            node.supp = set()
            node.rand = set()
            node.dom = set()
            node.passed_rules = set()
            node.mask_type = 'UKD'
            if isinstance(node, claripy.ast.Base):
                operations.add(node.op)
                if node.op == 'BVS' or node.op == 'BVV' or node.op == 'Concat':
                    leaf_node_compute(node, keys, node.rand, node.supp, node.dom)
                else:
                    perfect_total, seen_already = get_children_info(node, keys, operations)
                    xor_mask_rule(node, perfect_total)
                    unary_operation_rule(node)
                    bop_with_constant_rule(node)
                    more_than_two_children_rule(node, keys, operations)
                    independent_rule(node, keys)
                    bop_basic_rules(node)
                    bop_rule_five(node)
                    bop_rule_six(node)
                    if_rule(node)
        return node, operations
    else:
        raise Exception(node.op, ' is not a known operation.')


def get_vars(node):
    variables = []
    for child in node.args:
        if isinstance(child, claripy.ast.Base) and child.op != 'BVV':
            variables.insert(0, child)
    return variables


# The following are implementing rules that are taken from the paper 'Mitigating Power Side Channels during Compilation'
# by Wang, Sung, and Wang. https://arxiv.org/pdf/1902.09099.pdf

# this rule is mentioned but not numbered in the above paper.
def unary_operation_rule(node):
    if node.op in un_ops:
        for arg in node.args:
            if isinstance(arg, claripy.ast.Base):
                node.dom = arg.dom
                node.supp = arg.supp
                node.rand = arg.rand
                node.mask_type = arg.mask_type
                node.passed_rules.add('Unary Operation')


# rule 1 in the paper mentioned above
def xor_mask_rule(node, perfect_total):
    if node.op == '__xor__':
        node.dom = node.rand.intersection(perfect_total)
    else:
        node.dom = set()
    if len(node.dom) > 0:
        node.mask_type = 'RUD'
        node.passed_rules.add('XOR Mask Rule')
    return node.mask_type


# rule 2 in the paper mentioned above
def independent_rule(node, keys):
    keys_intersect = node.supp.intersection(keys)
    if node.mask_type != 'RUD' and len(keys_intersect) == 0:
        node.mask_type = 'SID'
        node.passed_rules.add('Independent of Key Rule')


# rules 3 and 4 in the paper mentioned above.
def bop_basic_rules(node):
    if node.op in bin_ops and node.mask_type != 'RUD':
        variables = get_vars(node)
        if len(variables) == 2:
            return rule_3_helper(variables[0], variables[1], node) or \
                   rule_3_helper(variables[1], variables[0], node)


def rule_3_helper(v1, v2, node):
    inter = v1.supp.intersection(v2.supp)
    if len(inter) == 0 and (v1.mask_type == 'RUD' or v1.mask_type == 'SID') and v2.mask_type == 'SID':
        node.mask_type = 'SID'
        node.passed_rules.add('Basic Binary Operation Rule')
        return True
    return False


# rule 5 in the paper mentioned above.
def bop_rule_five(node):
    if isinstance(node, claripy.ast.Base) and node.op in bin_ops_restricted:
        variables = get_vars(node)
        if len(variables) == 2:
            rule_five_helper(variables[0], variables[1], node)
            rule_five_helper(variables[1], variables[0], node)
        return node.mask_type


def rule_five_helper(v1, v2, node):
    is_empty = len(v1.dom - v2.supp) == 0
    one_perfect = v1.mask_type == 'RUD'
    same_perfect = v1.dom == v2.dom
    same_support = v1.supp == v2.supp
    if is_empty and one_perfect and same_perfect and same_support and node.mask_type != 'RUD':
        node.mask_type = 'SID'
        node.passed_rules.add('Binary Operations Rule 5')


# rule 6 in the paper mentioned above.
def bop_rule_six(node):
    if isinstance(node, claripy.ast.Base) and node.op in bin_ops_restricted and node.mask_type != 'RUD':
        variables = get_vars(node)
        if len(variables) == 2:
            return rule_six_helper(variables[0], variables[1], node)


def rule_six_helper(v1, v2, node):
    not_empty_condition = len(v1.dom - v2.supp) > 0 or len(v2.dom - v1.supp) > 0
    both_perfect = v1.mask_type == 'RUD' and v2.mask_type == 'RUD'
    if not_empty_condition and both_perfect and node.mask_type != 'RUD':
        node.mask_type = 'SID'
        node.passed_rules.add('Binary Operations Rule 6')
    return node.mask_type


# The rules below implement and further extend the ideas talked about in the paper mentioned above.

# If a node is not binary meaning there are more than 2 children that are not constants and the operation is
# associative, the node is split up into a binary tree. The node mask_type is evaluated as the mask_type of the top node
# of the generated binary tree.
def more_than_two_children_rule(node, keys, operations):
    if len(get_vars(node)) > 2 and node.op != 'If':
        new_tree = build_bin_tree(node, 0, node.op)
        compute_tables_helper(new_tree, keys, operations)
        node.mask_type = new_tree.mask_type
        node.passed_rules.add('Split Node Into Binary Tree')


# in the newly generated tree there is an __invert__ operation applied at each step. This changes the semantics of the
# tree but not the mask type. This is done in order to get around a property of claripy that combines binary trees.
# For this reason the generated binary tree should not be used for other purposes than determining mask_type.
def build_bin_tree(node, i, op):
    if op == '__and__':
        if len(node.args) - i == 2:
            tree = node.args[i] & node.args[i+1]
        else:
            tree = node.args[i] & build_bin_tree(node, i+1, op)
    elif op == '__add__':
        if len(node.args) - i == 2:
            tree = node.args[i] + node.args[i+1]
        else:
            tree = node.args[i] + build_bin_tree(node, i+1, op)
    elif op == '__xor__':
        if len(node.args) - i == 2:
            tree = node.args[i] ^ node.args[i+1]
        else:
            tree = node.args[i] ^ build_bin_tree(node, i+1, op)
    elif op == '__or__':
        if len(node.args) - i == 2:
            tree = node.args[i] | node.args[i+1]
        else:
            tree = node.args[i] | build_bin_tree(node, i+1, op)
    else:
        raise Exception(node.op, ' Is not covered by tree splitting')
    return ~tree


# if a binary operation is applied where the arguments are one ast and one or more constants the one variable ast is
# classified as SID or RUD, the node should be classified as SID
# If a node is RUD and you add or subtract 0 the node should remain RUD
def bop_with_constant_rule(node):
    if node.op in bin_ops and node.op != 'RUD':
        var = get_vars(node)
        if len(var) == 1:
            if len(node.args) == 2 and (node.op == '__sub__' or node.op == '__add__'):
                for child in node.args:
                    if child.op == 'BVV' and child.args[0] == 0:
                        for arg in node.args:
                            if arg.op != 'BVV':
                                node.dom = arg.dom
                                node.mask_type = arg.mask_type
                                node.passed_rules.add('Adding Zero')
                                return True
            node.mask_type = 'SID'
            node.passed_rules.add('Binary Operation With Constant')
    return False


# if a node has the operation 'If' the node should be classified as the least well masked of the 2 branches
# with RUD > SID > unknown.
def if_rule(node):
    if node.op == 'If':
        then_child = node.args[1]
        else_child = node.args[2]
        if then_child.mask_type == 'RUD' and else_child.mask_type == 'RUD':
            node.mask_type = 'RUD'
            node.passed_rules.add('SID by If rule')
        elif then_child.mask_type != 'UKD' and else_child.mask_type != 'UKD':
            node.mask_type = 'SID'
            node.passed_rules.add('SID by If rule')
    return node.mask_type


# A symbolic random bit vector that is not a key is RUD and a Constant is SID. A Constant should not be RUD as that
# would mean that __xor__ of any value with a contant would be RUD :(.
def leaf_node_compute(node, keys, randoms, support_v, uniform_dist):
    if node.op == 'BVS':
        leaf_value = node.args[0]
        if leaf_value not in keys:
            randoms.add(leaf_value)
            uniform_dist.add(leaf_value)
            node.mask_type = 'RUD'
            node.passed_rules.add('BVS not key is RUD')
        support_v.add(leaf_value)
    elif node.op == 'BVV':
        node.mask_type = 'SID'
        node.passed_rules.add('Constant is SID')
    elif node.op == 'Concat':
        node.mask_type = 'UKD'

# determines the number of asts that a node has in its arguments.
def num_children(node):
    children = 0
    for arg in node.args:
        if isinstance(arg, claripy.ast.Base):
            children += 1
    return children


# Prints every node of a ast recursively with generated information for each node.
def print_tree_detailed(leak):
    print("Printing tree with id ", leak.lp_id, '.')
    print(leak.lk_expr)
    print("-"*40)
    print('\n')
    
    n, ops = compute_tables(leak)
    print_tree_helper(n)
    print("")
    print("Operations: ", ops)
    print("Classified as ", n.mask_type, '.')
    print('\n\n\n\n')


def print_tree_helper(n):
    print("Node: ", n)
    print("Operation: ", n.op)
    print("Support Variables: ", n.supp)
    print("Random Variables: ", n.rand)
    print("Perfectly Masked Variables", n.dom)
    print("Mask Type ", n.mask_type)
    print("Passed Rules :", n.passed_rules)

    print("")
    print("")
    for node in n.args:
        if isinstance(node, claripy.ast.Base):
            print_tree_helper(node)


def print_check_masked(vbl):
    print("Now at leak with id", vbl.lp_id)
    n, ops = compute_tables(vbl)
    print("Tree: ", n)
    print("Top Operation: ", n.op)
    print("Support Variables: ", n.supp)
    print("Random Variables: ", n.rand)
    print("Perfectly Masked Variables", n.dom)
    print("Mask Classification: ", n.mask_type)
    print("Passed Rules: ", n.passed_rules)
    print("Operations: ", ops)
    print("")
    print("")
    if n.mask_type == 'UKD':
        base_unknown = find_root_unknown(n)
        print("UKD due to node: ", base_unknown)
        print("")
        print("")
    return n


# Returns a 4-tuple, namely (lk_expr, key, shr_vars, rnd_vars)
# lk_expr_sub is the tree we want to check.
# key_var is the secret variable in the tree.
# shr_vars is a list of share variables.
# rnd_vars is a list of random variables.
# In the context of this tree algorithm, both share and random variables should
# be treated as random.
def get_tree_vars(leak):
    key = claripy.BVS('c', leak.shares[0].length)
    key_expr = key
    for ci in leak.shares[:-1]:
        key_expr ^= ci
    lk_expr_sub = leak.lk_expr.replace(leak.shares[-1], key_expr, uf_app=True)
    shr_vars = leak.shares[0].variables
    rnd_vars = lk_expr_sub.variables - key.variables - shr_vars
    key_var = key.variables
    for i in range(1, len(leak.shares)):
        shr_vars = shr_vars.union(leak.shares[i].variables)
    return lk_expr_sub, key_var, shr_vars, rnd_vars


# prints summery of all asts in a list.
# Concludes a number / the total that are known to be masked with a certain classification.
def check_masked_trees(vbls):
    num_rud = 0
    num_sid = 0
    for vbl in vbls:
        tree = print_check_masked(vbl)
        if tree.mask_type == 'RUD':
            num_rud += 1
        elif tree.mask_type == 'SID':
            num_sid += 1
        print("--"*40)
    print("Trees RUD: ", num_rud,  "/", len(vbls))
    print("Trees SID: ", num_sid,  "/", len(vbls))


# uses a recursive dfs to determine the problem node that is causing the ast to be classified as unknown.
# to be used to figure out bad nodes or where rules need to be strengthened.
def find_root_unknown(node):
    if node.mask_type == 'UKD':
        for child in node.args:
            if isinstance(child, claripy.ast.Base) and child.mask_type == 'UKD':
                return find_root_unknown(child)
        return node
    raise Exception(node, " is masked with classification ", node.mask_type)
















