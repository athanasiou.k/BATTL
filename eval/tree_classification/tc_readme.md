# Tree Classification Tool Documentation
## Useful Functions:
### - classify(file_path :string, is_tbl :boolean)
#### Prints information about top node for each tree in one set of ast objects from .plp file.
### - classify_detailed(file_path :string, is_tbl :boolean, id :integer)
#### Prints information about each node from a specific tree from .plp file using id number.
### - find_root_unknown(ast)
#### Takes an ast classified as unknown and performs a dfs to find the deepest unknown node. This should be used to find "problem" nodes and to determine where rules need to be stronger.
### - compute_tables()
#### takes a leak(ast) object from .plp file. 
### - compute_tables_helper(ast, keys, operations
#### Takes in a claripy ast, the set of private key variables, and the current accumulation of variables

#### Needs to be called in a new function that just takes an ast. TODO.

#### Generates data structures (sets supp, rand, and dom) for each nodes and applies a set of rules to determine how the node should be classified. 
#### Will be recursively called on all children as well.

## Information available after running compute_tables on an ast:
### node.op = the operation performed at the top of this ast.
### node.supp = all the variables in support of this ast.
### node.rand = the random variables in support of this ast.
### node.dom = the perfectly masked variables in support of this ast.
### node.mask_type  = the classification of this node: one of {RUD, SID, unknown}
### node.passed_rules = the set of 'rules' that this node passed.
### node.operations = the operations used in this ast.
### node.args = the arguments of this node.

## Currently Implemented Rules:
##### Note: additional rules should be added to compute_tables_helper in leak_rules.py
### Rules 1, 2, 3, 4, 5, 6 and Uop from 'Mitigating Power Side Channels during Compilation' by Wang, Sung, and Wang https://arxiv.org/pdf/1902.09099.pdf
### If v is Constant : TYPE(v) = 'SID'
### If rule:
#### If op(v) == 'IF': and TYPE(branch1) == 'unknown' or TYPE(branch2) == 'unknown' then TYPE(v) = unknown.
#### Else If: op(v) == 'IF': and TYPE(branch1) == 'RUD' and TYPE(branch2) == 'RUD' then TYPE(v) = 'RUD'.
#### Else: TYPE(v) = 'SID'

## Known Bugs:

## Potential Improvements:
### - Change functions so they take in a claripy ast rather than a leak object!!!
### - More rules for binary operations are necessary the following tree is unknown:
#### node = x & y
#### where x.mask_type = 'SID' and x.supp =  {r, c}
#### where y.mask_type = 'SID and y.supp = {r, c, key}
### - More rules regarding constants might be possible ex. adding a constant to an RUD, rshift
### - Assign IDs to individual nodes in a tree in order to access parts of the tree.
