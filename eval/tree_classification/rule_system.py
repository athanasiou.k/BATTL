import batl.utils
import claripy.ast.base
import pickle
import IPython
import pydot
import pdb

RUD = 'RUD'
UKD = 'UKD'
SID = 'SID'
CST = 'CST'


#class set(set):

class varset(set):
    def __repr__(self):
        l = [e.split("_")[0] for e in self]
        return repr(l)

    
# Returns a 4-tuple, namely (lk_expr, key, shr_vars, rnd_vars)
# lk_expr_sub is the tree we want to check.
# key_var is the secret variable in the tree.
# shr_vars is a list of share variables.
# rnd_vars is a list of random variables.
# In the context of this tree algorithm, both share and random variables should
# be treated as random.
def get_tree_vars(leak):
    key = claripy.BVS('c', leak.shares[0].length)
    key_expr = key
    for ci in leak.shares[:-1]:
        key_expr ^= ci
    lk_expr_sub = leak.lk_expr.replace(leak.shares[-1], key_expr, uf_app=True)
    shr_vars = leak.shares[0].variables
    rnd_vars = lk_expr_sub.variables - key.variables - shr_vars
    txt_vars = {x for x in rnd_vars if x[0] == 'p'}
    rnd_vars = rnd_vars - txt_vars
    key_var = key.variables
    for i in range(1, len(leak.shares)):
        shr_vars = shr_vars.union(leak.shares[i].variables)
    return lk_expr_sub, key_var, shr_vars, rnd_vars



# lb_1 <- vbl.lk_expr.args[0].args[0].args[0]
def test_rules(file):
    tbls, vbls = batl.utils.get_lps(file)
    vbl = vbls[1]
    compute_aux_sets(vbl)
    classify_nodes(vbl)
    #print_aux_sets_lk_cls(vbl)
    la1 = vbl.lk_expr.args[0].args[0].args[0]
    print_rule_tree(vbl.lk_expr)
    print(la1)

    #    expr = vbl.lk_expr.args[0].args[0].args[0].args[2]
    
def print_rule_tree(expr):
    leaf_cache = set()
    for l in expr.leaf_asts():
        leaf_cache.add(l.cache_key)
    graph= pydot.Dot(graph_type='graph')
    worklist = [(expr, 0)]
    cnt = 0
    while worklist:
        runner, rid = worklist.pop()
        if runner.cache_key not in leaf_cache:
            runnerlabel = runner.op+"\ndom"+repr(runner.dom)+"\nunq"+repr(runner.unq)+"\n"+runner.lk_class
            runnernode = pydot.Node(str(rid), label=runnerlabel)
            graph.add_node(runnernode)
            for a in runner.args:
                cnt += 1
                if a.cache_key in leaf_cache:
                    nlabel = str(a.args[0])+"\ndom:"+repr(a.dom)+"\nunq"+repr(a.unq)+"\n"+a.lk_class
                else:
                    worklist.append((a, cnt))
                    nlabel = a.op+"\ndom"+repr(a.dom)+"\nunq"+repr(a.unq)+repr(a.unq)+"\n"+a.lk_class
                anode = pydot.Node(str(cnt), label=nlabel)
                graph.add_node(anode)
                graph.add_edge(pydot.Edge(runnernode, anode))
    graph.write_png('test.png')
    
    
    
# Given a Base.children_asts generator object, returns the reversed generator
# object. Useful for bottom-up traversal of the ast.
def bottom_up_ast(ast_iter):
    return reversed([e for e in ast_iter()])

def classify_nodes(pl):
    tree, keys, shares, rands = get_tree_vars(pl)
    leaf_cache = set()
    rands = shares.union(rands)
    expr = pl.lk_expr
    #expr= pl.lk_expr.args[0].args[0].args[0].args[0]
    # First Classify the leaves
    for l in expr.leaf_asts():
        leaf_cache.add(l.cache_key)
        if l.op == 'BVV':
            setattr(l, 'lk_class', CST)
        elif l.op == 'BVS' and l.args[0] in shares.union(rands):
            setattr(l, 'lk_class', RUD)
        else:
            setattr(l, 'lk_class', UKD)
    tree = list(bottom_up_ast(expr.children_asts))
    tree.append(expr)
    for n in tree:
        # Skip leaves
        if n.cache_key in leaf_cache:
            continue
        # Unary Ops
        if len(n.args) == 1:
            setattr(n, 'lk_class', n.args[0].lk_class)
        elif n.op == 'LShR' or n.op == '__lshift__':
            #pdb.set_trace()
            setattr(n, 'lk_class', n.args[0].lk_class)
        elif n.args[0].op == 'LShR' and n.args[1].op == '__lshift__' and (n.args[0].args[1].args[0] + n.args[1].args[1].args[0] == 32):
            setattr(n, 'lk_class', n.args[0].lk_class)
        elif n.op == 'Concat':
            same = n.args[0].lk_class
            for a in n.args:
                if a.lk_class != same:
                    raise Exception(n, 'has multiples classes')
            setattr(n, 'lk_class', same)
        elif n.op == '__xor__':
            lft = n.args[0]
            rgt = n.args[1]
            suppr_lft = n.args[0].variables.intersection(rands)
            suppr_rgt = n.args[1].variables.intersection(rands)
            if lft.lk_class == RUD and (lft.dom - suppr_rgt):
                lk_class_res = RUD
            elif rgt.lk_class == RUD and (rgt.dom - suppr_lft):
                lk_class_res = RUD
            elif rgt.lk_class == SID and lft.lk_class == SID and not suppr_lft.intersection(suppr_rgt):
                lk_class_res = SID
            elif not n.variables.intersection(keys):
                lk_class_res = SID
            else:
                lk_class_res = UKD
            suppr_acc = suppr_lft.union(suppr_rgt)
            unq_acc = n.args[0].unq.union(n.args[1].unq) -  suppr_lft.intersection(suppr_rgt)
            dom_acc = (n.args[0].dom.union(n.args[1].dom)).intersection(unq_acc) if n.op == '__xor__' else varset()
            for arg in n.args[2:]:
                # If the nth arg is a constant, skip and keep previous results.
                if arg.op == 'BVV':
                    continue
                suppr_rgt = arg.variables.intersection(rands)
                if lk_class_res == RUD and (dom_acc - suppr_rgt):
                    lk_class_res = RUD
                elif arg.lk_class == RUD and (arg.dom - suppr_acc):
                    lk_class_res = RUD
                elif arg.lk_class == SID and lk_class_res == SID and not suppr_acc.intersection(suppr_rgt):
                    lk_class_res = SID
                else:
                    lk_class_res = UKD
                unq_acc = unq_acc.union(arg.unq) - suppr_acc.intersection(suppr_rgt)
                dom_acc = dom_acc.union(arg.dom).intersection(unq_acc) if n.op == '__xor__' else varset()
                suppr_acc = suppr_acc.union(suppr_rgt)
            setattr(n, 'lk_class', lk_class_res)
        elif not n.variables.intersection(keys):
            setattr(n, 'lk_class', SID)
        else:
            lft = n.args[0]
            rgt = n.args[1]
            suppr_lft = n.args[0].variables.intersection(rands)
            suppr_rgt = n.args[1].variables.intersection(rands)
            if (((lft.lk_class == RUD and rgt.lk_class != UKD) or
                (rgt.lk_class == RUD and lft.lk_class != UKD)) and
                not suppr_lft.intersection(suppr_rgt)):
                lk_class_res = SID
            elif ((rgt.dom-suppr_lft).union(lft.dom-suppr_rgt)) and lft.lk_class == RUD and rgt.lk_class == RUD:
                lk_class_res = SID
            elif (lft.lk_class == SID) and (rgt.lk_class == SID) and (not suppr_lft.intersection(suppr_rgt)):
                lk_class_res = SID
            elif not n.variables.intersection(keys):
                lk_class_res = SID
            else:
                lk_class_res = UKD
            suppr_acc = suppr_lft.union(suppr_rgt)
            unq_acc = n.args[0].unq.union(n.args[1].unq) -  suppr_lft.intersection(suppr_rgt)
            dom_acc = (n.args[0].dom.union(n.args[1].dom)).intersection(unq_res) if n.op == '__xor__' else varset()
            for arg in n.args[2:]:
                # If the nth arg is a constant, skip and keep previous results.
                if arg.op == 'BVV':
                    continue
                suppr_rgt = arg.variables.intersection(rands)
                if (((lk_class_res == RUD and arg.lk_class != UKD) or
                     (arg.lk_class == RUD and lk_class_res != UKD)) and
                    not suppr_acc.intersection(suppr_rgt)):
                    lk_class_res = SID
                elif ((arg.dom-suppr_acc).union(dom_acc-suppr_rgt)) and lk_class_res == RUD and arg.lk_class == RUD:
                    lk_class_res = SID
                elif (lk_class_res == SID) and (arg.lk_class == SID) and (not suppr_acc.intersection(suppr_rgt)):
                    lk_class_res = SID
                elif not n.variables.intersection(keys):
                    lk_class_res = SID
                else:
                    lk_class_res = UKD
                suppr_rgt = arg.variables.intersection(rands)
                unq_res = unq_res.union(arg.unq) - suppr.intersection(suppr_rgt)
                dom_res = dom_res.union(arg.dom).intersection(unq_res) if n.op == '__xor__' else varset()
                suppr_acc = suppr.union(suppr_rgt)
            setattr(n, 'lk_class', lk_class_res)

def compute_aux_sets(pl):
    tree, keys, shares, rands = get_tree_vars(pl)
    leaf_cache = set()
    rands = varset(shares.union(rands))
    expr = pl.lk_expr
    #expr= pl.lk_expr.args[0].args[0].args[0].args[0]
    for l in expr.leaf_asts():
        leaf_cache.add(l.cache_key)
        unq = varset()
        dom = varset()
        if l.op == 'BVS' and l.args[0] in rands:
            unq.add(l.args[0])
            dom.add(l.args[0])
        setattr(l, 'unq', unq)
        setattr(l, 'dom', dom)
    #IPython.embed()
    tree = list(bottom_up_ast(expr.children_asts))
    tree.append(expr)
    for n in tree:
        if n.cache_key in leaf_cache:
            continue
        # Unary Ops
        if len(n.args) == 1:
            n.unq = n.args[0].unq
            n.dom = n.args[0].dom
        # N-ary Ops
        else:
            # If constant, ignore and take dom, unq from other arg.
            if n.args[0].op == 'BVV':
                unq_res = n.args[1].unq
                dom_res = n.args[1].dom
            elif n.args[1].op == 'BVV':
                unq_res = n.args[0].unq
                dom_res = n.args[0].dom
            # None of the two args is a constant.
            elif n.args[0].op == 'LShR' and n.args[1].op == '__lshift__' and (n.args[0].args[1].args[0] + n.args[1].args[1].args[0] == 32):
                unq_res = n.args[0].unq
                dom_res = n.args[0].dom
            else:
                suppr_lft = varset(n.args[0].variables.intersection(rands))
                suppr_rgt = varset(n.args[1].variables.intersection(rands))
                unq_res = varset(n.args[0].unq.union(n.args[1].unq) - suppr_lft.intersection(suppr_rgt))
                dom_res = varset(n.args[0].dom.union(n.args[1].dom).intersection(unq_res)) if n.op == '__xor__' else varset()
#                dom_res = n.args[0].dom.union(n.args[1].dom).intersection(unq_res) if n.op == '__xor__' else varset()
                suppr = suppr_lft.union(suppr_rgt)
            for arg in n.args[2:]:
                # If the nth arg is a constant, skip and keep previous results.
                if arg.op == 'BVV':
                    continue
                suppr_rgt = varset(arg.variables.intersection(rands))
                unq_res = varset(unq_res.union(arg.unq) - suppr.intersection(suppr_rgt))
                dom_res = varset(dom_res.union(arg.dom).intersection(unq_res)) if n.op == '__xor__' else varset()
                suppr = suppr.union(suppr_rgt)
            n.unq = unq_res
            n.dom = dom_res
            

def print_aux_sets_lk_cls(pl):
    tree, keys, shares, rands = get_tree_vars(pl)
    tree = list(bottom_up_ast(pl.lk_expr.children_asts))
    tree.append(pl.lk_expr)
    for n in tree:
        print(n)
        print("unq:\t", n.unq)
        print("dom:\t", n.dom)
        print("cls:\t", n.lk_class) if hasattr(n, 'lk_class') else ()
        print(80*"-")
    print("keys:\t", keys)
    print("shares:\t", shares)
    print("rands:\t", rands)


