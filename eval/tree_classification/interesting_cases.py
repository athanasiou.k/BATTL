import leak_rules

file_one = 'secinvO0.plp'
file_two = 'secinvO1.plp'

print('Printing an unknown tree with an XOR at the top. Should be unknown with the given rules.')
leak_rules.classify_detailed(file_one, False, 0)

print('\n\n\n\n')

print('Printing an unknown tree that does not pass any of the BOP rules implemented so far. Marked as unknown')
leak_rules.classify_detailed(file_one, True, 9)

print('\n\n\n\n')

print("Printing a good simple example of a tree marked as SID by Unary operation rules and BOP rules.")
leak_rules.classify_detailed(file_one, True, 1)

print("Printing an example of the If rule working on the top node")
leak_rules.classify_detailed(file_one, True, 2)


