from batl.dwarf_decode_address import process_file, bytes2str
import os
import pickle
import IPython
import numpy as np
import pdb

tbl_qemu_eval_code="""@leak in {4} 
	push	{{ {0}, {1}, {2},  {3} }}
	mrs 	{3}, APSR
	mov	{2}, {4}
{6}
	eor	{2}, {2}, {4}
	ldr 	{0}, =leakage_visit_counter
	ldr	{1}, [{0}]
	add	{1}, {1}, #1
	strb	{1}, [{0}]
	cmp 	{1}, #{5}
	bne	.cont
	ldr 	{1}, =leakage_value
	str	{2}, [{1}]
.cont:
	msr 	APSR_nzcvq, {3}
	pop	{{ {0}, {1}, {2}, {3} }}
"""

qemu_eval_code ="""@leak in {3}
	push 	{{ {0}, {1}, {2} }}
	mrs 	{2}, APSR
	ldr 	{0}, =leakage_visit_counter
	ldr	{1}, [{0}]
	add	{1}, {1}, #1
	strb	{1}, [{0}]
	cmp 	{1}, #{4}
	bne	.cont
	ldr 	{0}, =leakage_value
	str	{3}, [{0}]
.cont:
	msr 	APSR_nzcvq, {2}
	pop	{{ {0}, {1}, {2} }}
"""

# no touch: int list of register indices not to be used
# num_regs: int number of needed registers
# return: str list of num_regs registers not in no_touch
def reg_list(no_touch, num_regs):
    i = 0
    acc = []
    while len(acc) != num_regs:
        if i not in no_touch:
            acc.append("r"+str(i))
        i += 1
    return acc

condition_codes = ['ls', 'hi', 'cs', 'cc', 'ne', 'eq', 'lt']
def in_ite_block(insn_str):
    opcode = insn_str.split()[0]
    if opcode == "bne":
        return False
    return any(opcode.endswith(cc) for cc in condition_codes)

def in_it_block(insn_str, line, contents):
    opcode = insn_str.split()[0]
    return any(opcode.endswith(cc) for cc in condition_codes) and 'it' in contents[line-2] and 'ite' not in contents[line-2]
        
def first_of_ite_block(line, contents):
    return 'ite' in contents[line-2]

def is_multipop(insn_str):
    return 'pop' in insn_str and ',' in insn_str

def qemu_ila_of_lp(plp_file, elf_file, lp_id, leak_type):
    reg_name_map = {
        'lr' : 'r14'
        }
    with open(plp_file, 'rb') as f:
        tbl_leaky_points, vbl_leaky_points = pickle.load(f)

    target_lp = None 
    if leak_type  == "TBL":
        for lp in tbl_leaky_points:
            if lp.lp_id == lp_id:
                target_lp = lp
    elif leak_type == "VBL":
        for lp in vbl_leaky_points:
            if lp.lp_id == lp_id:
                target_lp = lp

    # print(target_lp.lp_id);
    file_name, file_line = process_file(elf_file, target_lp.new_write_addr)
    asm_file = os.path.join(os.path.dirname(elf_file), file_name.decode('utf-8'))
    with open (asm_file, 'r+') as af:
        contents = af.readlines()


    reg_name = reg_name_map[target_lp.reg_name] if target_lp.reg_name in reg_name_map else target_lp.reg_name
    reg_id = int(reg_name[1:])
    if leak_type == "VBL":
        required_regs = 3
        # First N=required_regs arguments to format are the push/pop registers
        no_touch_regs = list(set([reg_id]).union(set([int(r[1:]) if r[0]=='r' else None for r in target_lp.read_set])))
        rargs = reg_list(no_touch_regs, required_regs)
        rargs.sort()
        # Fourth argument is the target register
        rargs = rargs + [target_lp.reg_name]
        # Fifth argument is the leakage visit counter
        fargs = rargs + [target_lp.visit_cnt]
        # print(qemu_eval_code.format(*fargs))
        if in_ite_block(target_lp.insn_str) and first_of_ite_block(file_line, contents):
            contents.insert(file_line+1, qemu_eval_code.format(*fargs))
        elif is_multipop(target_lp.insn_str):
            if target_lp.reg_name+',' in target_lp.insn_str:
                pop_wo_reg = "\t"+ target_lp.insn_str.replace(target_lp.reg_name+',', '') + "\n"
            else:
                pop_wo_reg = "\t"+ target_lp.insn_str.replace(target_lp.reg_name, '') + "\n"
            new_pop = "\tpop\t{{ {0} }}\n".format(target_lp.reg_name)
            contents.insert(file_line-1, new_pop)
            contents[file_line] = pop_wo_reg
            contents.insert(file_line-1, qemu_eval_code.format(*fargs))
        else:
            contents.insert(file_line, qemu_eval_code.format(*fargs))
    elif leak_type == "TBL":
        required_regs = 4
        # the def and use registers of the TBL inducing instruction must not be touched
        no_touch_regs = list(set([reg_id]).union(set([int(r[1:]) if r[0]=='r' else None for r in target_lp.read_set])))
        # First N=required_regs arguments to format are the push/pop registers
        rargs = reg_list(no_touch_regs, required_regs)
        # rargs = ["r"+str(i) if i not in no_touch_regs else "r"+str(required_regs+i) for i in range(required_regs)]
        rargs.sort()
        # Fifth argument is the target register
        rargs = rargs + [target_lp.reg_name]
        # Sixth argument is the leakage visit counter
        fargs = rargs + [target_lp.visit_cnt]
        # Seventh argument is the TBL inducing instruction
        # If we are in an ite block, all 3 instructions are the seventh argument
        if in_ite_block(target_lp.insn_str):
            if first_of_ite_block(file_line, contents):
                insns = contents[file_line-2] + contents[file_line-1] + contents[file_line]
                del contents[file_line-2:file_line+1]
                file_line_to_insert = file_line - 2
            else:
                insns = contents[file_line-3] + contents[file_line-2] + contents[file_line-1]
                del contents[file_line-3:file_line]
                file_line_to_insert = file_line - 3
            fargs = fargs + [insns]
            contents.insert(file_line_to_insert, tbl_qemu_eval_code.format(*fargs))
        elif in_it_block(target_lp.insn_str, file_line, contents):
            insns = contents[file_line-2] + contents[file_line-1] + contents[file_line]
            del contents[file_line-2:file_line+1]
            file_line_to_insert = file_line - 2
            fargs = fargs + [insns]
            contents.insert(file_line_to_insert, tbl_qemu_eval_code.format(*fargs))
        elif is_multipop(target_lp.insn_str):
            # Split the multipop to one single pop and on multipop withouth the target register.
            # Then apply the wrapper code as usual
            if target_lp.reg_name+',' in target_lp.insn_str:
                pop_wo_reg = "\t"+ target_lp.insn_str.replace(target_lp.reg_name+',', '') + "\n"
            else:
                pop_wo_reg = "\t"+ target_lp.insn_str.replace(target_lp.reg_name, '') + "\n"
            new_pop = "\tpop\t{{ {0} }}\n".format(target_lp.reg_name)
            contents.insert(file_line-1, new_pop)
            contents[file_line] = pop_wo_reg
            fargs = fargs + [new_pop]
            contents[file_line-1] = tbl_qemu_eval_code.format(*fargs)
        else:
            insns = "\t"+target_lp.insn_str
            fargs = fargs + [insns]
            contents[file_line-1] = tbl_qemu_eval_code.format(*fargs)
    head, tail = os.path.split(asm_file)
    asm_file = os.path.join(head, leak_type+str(lp_id), tail)

    with open(asm_file, "w") as f2:
        f2.writelines(contents)


        
def print_aux_files(plp_file, elf_file, lp_id, leak_type):

    with open(plp_file, 'rb') as f:
        tbl_leaky_points, vbl_leaky_points = pickle.load(f)

    target_lp = None
    if leak_type  == "TBL":
        for lp in tbl_leaky_points:
            if lp.lp_id == lp_id:
                target_lp = lp
    elif leak_type == "VBL":
        for lp in vbl_leaky_points:
            if lp.lp_id == lp_id:
                target_lp = lp

    head, tail = os.path.split(plp_file)
    aux_file = os.path.join(head, leak_type+str(lp_id), "AUX")
    num_rands = len(target_lp.lk_expr.variables) - len(target_lp.shares)
    share=target_lp.shares[0]
    key_idx=share.args[0].split("_")[0][1:]
    file_name, _ = process_file(elf_file, target_lp.new_write_addr)
    with open(aux_file, "w") as f:
        f.write("-D NUM_RANDS={0}\n".format(num_rands))
        f.write("-D KEY_IDX={0}\n".format(key_idx))
        f.write(file_name.decode('utf-8'))

def gather_results(results_file, plp_file, lp_id, leak_type, *args):
    with open(plp_file, 'rb') as f:
        tbl_leaky_points, vbl_leaky_points = pickle.load(f)
    
    target_lp = None
    if leak_type  == "TBL":
        for lp in tbl_leaky_points:
            if lp.lp_id == lp_id:
                target_lp = lp
    elif leak_type == "VBL":
        for lp in vbl_leaky_points:
            if lp.lp_id == lp_id:
                target_lp = lp

    out_word_size = 32
    out_word_range = 2**32
    in_word_size = 8
    in_word_range = 2**in_word_size
    total_vars = len(target_lp.lk_expr.variables)
    if total_vars <= 3 and 'hoaes' not in plp_file:
        space_size_wo_key = in_word_range**(total_vars-1)
    elif  args != ():
        print("sample size:{0}".format(args[0]))
        space_size_wo_key = int(args[0])
    else:
        space_size_wo_key =  10000
    with open(results_file, 'r') as f:
        lines = f.readlines();
    i = 0
    u_table = np.zeros((in_word_range, out_word_size+1), dtype=np.int)

    for line in lines:
        res = int(line.split(":")[1])
        keyval = int(line.split(":")[0].split(",")[0])
        hwval = int(line.split(":")[0].split(",")[1])
        u_table[keyval][hwval] += res
    print(np.sum(u_table))
    np.savetxt("qemu_V_HW.out", u_table, delimiter=',', fmt='%d')
    expect_of_g = np.zeros(in_word_range)
    for i in range(0, in_word_range):
        for j in range(1, out_word_size+1):
            expect_of_g[i] = expect_of_g[i] + j*u_table[i, j]/space_size_wo_key

    # Calculating confusion coefficient for different kg
    confusion_vector = np.zeros(in_word_range)
    kc = 10
    for k in range(0, in_word_range):
        Each_case = np.zeros(in_word_range)
        for p in range(0, in_word_range):
            Each_case[p] = (expect_of_g[k ^ p] - expect_of_g[kc ^ p])**2
        confusion_vector[k] = Each_case.mean()

    print(sum(confusion_vector)/(in_word_range-1))
