import eval.mc.coll_check as coll_check
import eval.mc.gfmul as gfmul
import eval.mc.bit_blaster as bb
from z3 import *
import sys
import subprocess

word_size = 8

c = BitVec('c', word_size)
c0 = BitVec('c0', word_size)
c0p = BitVec('c0p', word_size)
r = BitVec('r', word_size)
rp = BitVec('rp', word_size)

# Expressions derived from TIFO study:

# E1=LP0
# E1: log[r ^ sq[c ^ c0]] ^ log[sq[c0] ^ r]
# ILA(E1)=0.138
# vars(E1)=3
sqcc0 = gfmul.lookup2if(c ^ c0, "sq")
log1 = gfmul.lookup2if(r ^ sqcc0, "log")
sqc0 = gfmul.lookup2if(c0, "sq")
log2 = gfmul.lookup2if(sqc0 ^ r, "log")
e1 = log1 ^ log2

sqcc0p = gfmul.lookup2if(c ^ c0p, "sq")
log1p = gfmul.lookup2if(rp ^ sqcc0p, "log")
sqc0p = gfmul.lookup2if(c0p, "sq")
log2p = gfmul.lookup2if(sqc0p ^ rp, "log")
e1p = log1p ^ log2p


# E2=LP2
# E2: (c ^ c0) | (-(c ^ c0)) >> 7 ^ (-c0)
# ILA(E2)=0.0004
# vars(E2)=2
e2 = (((c ^ c0)|(-(c^c0))) >> 7) ^ (-c0)
e2p = (((c ^ c0p)|(-(c^c0p))) >> 7) ^ (-c0p)


# E3=LP3
# E3: gfmul(c0, sq[c0] ^ r) ^ alog[log[c0^c] + log[sq[c0^c] ^r]]
# ILA(E3)=0.12
# vars(E3)=3

lmul = gfmul.mul(c0, sqc0 ^ r)
llog = gfmul.lookup2if(c0 ^ c, "log")
alog = gfmul.lookup2if(llog + log1, "alog")
e3 = lmul ^ alog


lmulp = gfmul.mul(c0p, sqc0p ^ rp)
llogp = gfmul.lookup2if(c0p ^ c, "log")
alogp = gfmul.lookup2if(llogp + log1p, "alog")
e3p = lmulp ^ alogp

# Expressions derived from SIFO

# E4=LP0
# E4: sq[c^c0]^ sq[c0]
# ILA(E4)=4.01
# vars(E4)=2

e4 = sqcc0 ^ sqc0
e4p = sqcc0p ^ sqc0p

# TODO: 5 var (x2 and y0) xor (x0 and y2) xor x1
# whern and is gfmul

# e4check = coll_check.Checker("e4", e4, e4p, [c, c0], [c, c0, c0p])
# for i in [1]:#range(9):
#     print("###{0}###".format(i))
#     q = e4check.mc_q(i, hw=True)
#     c = e4check.mc_c(i)
#     print("q={0}\tc={1}".format(q, c))

# e2check = coll_check.Checker("e2", e2, e2p, [c, c0], [c, c0, c0p])
# for i in range(9):
#     print("###{0}###".format(i))
#     q = e2check.mc_q(i, hw=True)
#     c = e2check.mc_c(i)
#     print("q={0}\tc={1}".format(q, c))

e1check = coll_check.Checker("e1", e1, e1p, [c, c0, r], [c, c0, c0p, r, rp])
for i in range(9):
    print("###{0}###".format(i))
    q = e1check.mc_q(i, hw=True)
    c = e1check.mc_c(i)
    print("q={0}\tc={1}".format(q, c))
    
