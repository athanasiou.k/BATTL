import eval.mc.gfmul as gfmul
import eval.mc.bit_blaster as bb
import sys
import subprocess
from z3 import *
import operator as op
from functools import reduce

class Checker(object):
    def __init__(self, name, expr, expr_p, bv_vars, bv_vars_p, word_size):
        self.name = name
        self.expr = expr
        self.expr_p = expr_p
        self.bv_vars = bv_vars
        self.bv_vars_p = bv_vars_p
        self.word_size = word_size

    @staticmethod
    def ncr(n, r):
        r = min(r, n-r)
        numer = reduce(op.mul, range(n, n-r, -1), 1)
        denom = reduce(op.mul, range(1, r+1), 1)
        return numer / denom

    @staticmethod
    def _call_mc(benchmark_name, max_count = None):
        dimacs_file = benchmark_name+"_dimacs.cnf"
        scope_file = benchmark_name+"_scope.var"
        count_mode = "-countMode=2"
        projection = "-projection="+scope_file
        #solver = "sharpCDCL"
        solver = "approxmc"
        if max_count:
            max_model = "-maxModel="+str(max_count)
            args = (solver, count_mode, max_model, projection, dimacs_file)
        else:
            #args = (solver, count_mode, projection, dimacs_file)
            args = (solver, dimacs_file)
        print(args)
        popen = subprocess.Popen(args, stdout=subprocess.PIPE)
        popen.wait()
        output = popen.stdout.read().decode("utf-8")
        out_lines = output.split("\n")
        res_line = out_lines[-2]
        print(res_line)
        res=res_line[32:]
        numnops=res.split(' ')
        a = int(numnops[0])
        b = int(numnops[2][2:])
        models = a*2**b
        # print(a, b)
        # print(numnops)
        # models = output.strip().split(" ")[2]
        return int(models)

    
    # The so called "gosper hack" to find numbers with specific HW in ascending order.
    @staticmethod
    def gosper(hw_val, word_size):
        if hw_val == 0:
            return [0]
        smallest = int("1"*hw_val, 2)
        next_val = smallest
        result = []
        while next_val < 2**word_size:
            #print(next_val, "{:b}".format(next_val))
            result.append(next_val)
            c = next_val & -next_val
            r = next_val + c
            next_val = (((r^next_val) >> 2) // c) | r
        return result

    @staticmethod
    def coll_cutoff(q, word_size):
        B = 2**word_size
        max_c = 2*B*Checker.ncr(q//word_size, 2) + q
        return max_c
              
    def mc_q(self, out_val, hw=True):
        if hw:
            hw_vals = self.gosper(out_val, self.word_size)
            hw_cons = []
            for v in hw_vals:
                hw_cons.append(self.expr == BitVecVal(v, self.word_size))
            q = [Or(hw_cons)]
        else:
            q = self.expr == gfmul.bv_vals[out_val]

        benchmark_name = self.name + "q" + str(out_val)
        bb.bit_blast(q, self.bv_vars, self.word_size, benchmark_name)
        q = self._call_mc(benchmark_name)
        return q

    def mc_c(self, out_val, q=None, hw=True):
        if hw:
            hw_vals = self.gosper(out_val, self.word_size)
            hw_cons = []
            hw_consp = []
            for v in hw_vals:
                hw_cons.append(self.expr == gfmul.bv_vals[v])
                hw_consp.append(self.expr_p == gfmul.bv_vals[v])
            c = [Or(hw_cons), Or(hw_consp)]
        else:
            c = [(self.expr == gfmul.bv_vals[out_val]),
                 (self.expr_p == gfmul.bv_vals[out_val])]
        benchmark_name = self.name + "c" + str(out_val)
        bb.bit_blast(c, self.bv_vars_p, self.word_size, benchmark_name)
        if q is not None:
            max_c = self.coll_cutoff(q, word_size)
        else:
            max_c = None
        c = self._call_mc(benchmark_name, max_count=max_c)
        return c
