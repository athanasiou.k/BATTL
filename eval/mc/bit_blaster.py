from z3 import *
import datetime

def _var2num(var):
    """
    Gets a z3 smt2 bitblasted var of the form k!varnum and returns varnum as an
    integer.

    :param z3_var var: z3 smt2 bitblasted var of the form k!varnum
    
    :return int varnum
    """
    return int(var[2:])

def bit_blast(expr, bv_vars, word_size, annot):
    """
    Outputs the bitblasted dimacs and the projection scope of a z3 bitvector
    expression in two separate .cnf and .var files.
    
    :param z3_expr expr: The z3 BitVecSort expression to be bitblasted.
    :param list of z3_var bv_vars: List of BitVecs included in expr.
    :param int word_size: The word size of the BitVecs.
    :param str annot: prefix of the dimacs and scope files to be generated.

    :return None
    """
    print("[{0}]: Bit-Blasting..".format(datetime.datetime.now().time()))
    g = Goal()
    
    bitmap = {}
    for var in bv_vars:
        var_name = var.sexpr()
        for i in range(word_size):
            bitmap[(var,i)] = Bool(var_name+str(i))
            mask = BitVecSort(word_size).cast(math.pow(2,i))
            g.add(bitmap[(var,i)] == ((var & mask) == mask))
    
    g.add(expr)
    #print(g)
    # z3 k!num expr -> z3 Boolvar expr
    bb2z3 = dict()

    dimacs_lit_counter = 1
    # int(num) -> int map from bit-blasted variable num to dimacs variable num
    bb2dim = dict()
    t = Then('simplify', 'bit-blast', 'tseitin-cnf')
    subgoal = t(g)


    og_vars = set(bitmap.values())

    # I am checking whether an ast node is an or, not or a literal using kind
    # identifier. It could also be done by matching on the ast node's names.
    # KIND map
    # Or  -> 262
    # Not -> 265
    # k!  -> 2354
    OR_KIND = 262
    NOT_KIND = 265
    K_KIND = 2354
    print("[{0}]: Compiling to dimacs..".format(datetime.datetime.now().time()))
    cnf = []

    for c in subgoal[0]:
        clause = []
        if c.decl().kind() == OR_KIND:
            for lit in c.children():
                if lit.decl().kind() == NOT_KIND and lit.arg(0) in og_vars:
                    bb_var_name = c.children()[0].sexpr()
                    bb2z3[bb_var_name] = lit.arg(0)
                    break
                elif lit in og_vars:
                    break
                elif lit.decl().kind() == NOT_KIND:
                    bb_lit = _var2num(lit.arg(0).sexpr())
                    if bb_lit not in bb2dim.keys():
                        bb2dim[bb_lit] = dimacs_lit_counter
                        dimacs_lit_counter += 1
                    clause.append(-bb2dim[bb_lit])
                else:
                    bb_lit = _var2num(lit.sexpr())
                    if bb_lit not in bb2dim.keys():
                        bb2dim[bb_lit] = dimacs_lit_counter
                        dimacs_lit_counter += 1
                    clause.append(bb2dim[bb_lit])
            else:
                clause.append(0)
                cnf.append(clause)
        elif c.decl().kind() == NOT_KIND:
            bb_lit = _var2num(c.arg(0).sexpr())
            if bb_lit not in bb2dim.keys():
                bb2dim[bb_lit] = dimacs_lit_counter
                dimacs_lit_counter += 1
            clause.append(-bb2dim[bb_lit])
            clause.append(0)
            cnf.append(clause)
        else:
            bb_lit = _var2num(c.sexpr())
            if bb_lit not in bb2dim.keys():
                bb2dim[bb_lit] = dimacs_lit_counter
                dimacs_lit_counter += 1
            clause.append(bb2dim[bb_lit])
            clause.append(0)
            cnf.append(clause)


            
    print("[{0}]: Printing dimacs w/ projection..".format(datetime.datetime.now().time()))
            
    num_clauses = len(cnf)
    cnf = [' '.join(str(lit) for lit in clauses) for clauses in cnf]
    cnf = '\n'.join(cnf)+'\n'

    # Find the literals that correspond to z3 bvs, i.e. the sampling set.
    # Also fine the  projection, i.e. literals that do not correspond to any bv_var.

    sampling_set = [ bb2dim[_var2num(bb)]for bb in bb2z3.keys()]

    # print(bb2dim)
    # print(bb2z3)
    # print(bitmap)
    with open('{0}_dimacs.cnf'.format(annot), 'w') as dimacs_file:
        ind = ' '.join(str(x) for x in sampling_set)
        dimacs_file.write("c ind {0} 0\n".format(ind))
        dimacs_file.write("p cnf {0} {1}\n".format(dimacs_lit_counter-1, num_clauses))
        dimacs_file.write(cnf)

    print("[{0}]: Computing and printing projection..".format(datetime.datetime.now().time()))
    # print(lits_in_bvs)
    # The rest of the literals are the projection
    projection = set(bb2dim.values()) - set(sampling_set)
    #print(projection)
    with open('{0}_scope.var'.format(annot), 'w') as scope_file:
        for lit in projection:
            scope_file.write(str(lit)+"\n")
