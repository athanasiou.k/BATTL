from z3 import *
import eval.tables

word_size = 8
bv_vals = []

for i in range(2**word_size):
    bv_vals.append(BitVecVal(i, word_size))

def _if_rec(bv_expr, i, ite_acc, lu_name):
    if i < 0:
        return ite_acc
    lu_expr = None
    if lu_name is "log":
        lu_expr = bv_vals[eval.tables.log_t[i]]
    elif lu_name is "alog":
        lu_expr = bv_vals[eval.tables.alog_t[i]]
    elif lu_name is "sq":
        lu_expr = bv_vals[eval.tables.sq[i]]
    else:
        raise ValueError('Unsupported lookup name')
    new_ite = If(bv_expr == bv_vals[i], lu_expr, ite_acc)
    if i == 0:
        return new_ite
    else:
        return _if_rec(bv_expr, i-1, new_ite, lu_name)

def lookup2if(expr, lu_name):
    """
    Returns the z3 expression of a table lookup operation in GF(256).
    The lookup is implemented via nested If-Then-Else statements.

    :param z3_expr expr: The z3 expression used as index for the table lookup.
    :param str lu_name: The name of the GF(256) lookup operation.

    :return z3_expr lu_name[expr]: The z3 expression of the table lookup.
    """
    if lu_name is "log":
        lu_expr_last = bv_vals[eval.tables.log_t[(2**word_size) - 1]]
        lu_expr_sec_last = bv_vals[eval.tables.log_t[(2**word_size) - 2]]
    elif lu_name is "alog":
        lu_expr_last = bv_vals[eval.tables.alog_t[(2**word_size) - 1]]
        lu_expr_sec_last = bv_vals[eval.tables.alog_t[(2**word_size) - 2]]
    elif lu_name is "sq":
        lu_expr_last = bv_vals[eval.tables.sq[(2**word_size) - 1]]
        lu_expr_sec_last = bv_vals[eval.tables.sq[(2**word_size) - 2]]
    else:
        raise ValueError('Unsupported lookup name')
    last_ite = If(expr == bv_vals[(2**word_size) - 2],
                  lu_expr_sec_last,
                  lu_expr_last)
    return _if_rec(expr, (2**word_size) - 3, last_ite, lu_name)

def mul(a, b):
    """
    Returns a z3 expression of multiplication in GF(256) implemented with 
    log and alog lookup tables.

    :param z3_expr a: First operand of the multiplication.
    :param z3_expr b: Second operand of the multiplication.

    :return z3_expr GFmul(a, b): GF(256) multiplication expression.
    """
    it = BitVec('t', word_size)
    iss = BitVec('s', word_size)
    ic = BitVec('c', word_size)
    mul = BitVec('mul', word_size)
    bv_vars = [a,b]

    z = If(a == bv_vals[0], bv_vals[0], If(b==bv_vals[0], bv_vals[0], bv_vals[1])) 
    t = lookup2if(a, "log")
    ss = lookup2if(b, "log") + t
    c = If(ULT(ss, t), bv_vals[1], bv_vals[0])
    r = lookup2if(c+ss, "alog")
    res = If(a == bv_vals[0], bv_vals[0], If(b==bv_vals[0], bv_vals[0], r))

    return res
